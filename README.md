**Canvas / Ed Discussion group number: Group 17**

## Project Name: Bluegrass Cancer Navigator

**Website URL:** [https://www.bluegrasscancernav.me/](https://www.bluegrasscancernav.me/)

**Backend API Link:** [https://www.api.bluegrasscancernav.me/](https://www.api.bluegrasscancernav.me/)

**API Docs Link:** [https://documenter.getpostman.com/view/32889204/2sA2r3Z69p](https://documenter.getpostman.com/view/32889204/2sA2r3Z69p)

**GitLab Pipeline:** [https://gitlab.com/davindertoor77/cs373-group-17/-/pipelines](https://gitlab.com/davindertoor77/cs373-group-17/-/pipelines)

**PechaKucha Presentation:** [https://www.youtube.com/watch?v=dGVBS_LJoz8](https://www.youtube.com/watch?v=dGVBS_LJoz8)

### Names EIDs, and Gitlab IDs of the team members
| Name            | GitLabID | EID |
| ---------------- | --------------- | ------ |
| Numan Osagie                     | @nosagie1                      | noo248       |
| Simon Stone                      | @simonstone                    | sds4253      |
| Donald Thai                      | @donaldthai                    | dat3226      |
| Davinderpal Toor                 | @davindertoor77                | dst775       |
| HeSong Wang                      | @Hersoncpp                     | Hw23233      |

### GIT SHA
| Phase | SHA |
| ----- | --- |
| 1          |ddcdb153cb40b179ef17b6d8ddba579af8d0a509|
| 2          |004cd5234ea8c1612de054c09635f1c822476e3f|
| 3          |ab75e73907c926a2a8e6d82e1df69ae8ecad0f2f|
| 4          |2e6c27d567f18edec0815c377613f5019b8d5324|

### Phase Leader
| Phase        | Phase Leader |
| ----- | ------------ |
| 1          |Davinderpal|
| 2          |HeSong|
| 3          |Donald|
| 4          |Simon|

Responsibilities: Plan meetings and ensure members are completing their work on time and helping out if possible.

## Estimated/Actual Completion Time
### Phase 1
| Member    | Estimated | Actual |
| ----------- | --------- | ------ |
| Numan                  | 12                 |14|
| Simon                  | 10                 |12|
| Donald                 | 06                 |14|
| Davinderpal            | 10                 |22|
| HeSong                 | 08                 |15|

### Phase 2
| Member    | Estimated | Actual |
| ----------- | --------- | ------ |
| Numan                  | 16                 |14|
| Simon                  | 15                 |18|
| Donald                 | 14                 |15|
| Davinderpal            | 17                 |30|
| HeSong                 | 15                 |17|


### Phase 3
| Member    | Estimated | Actual |
| ----------- | --------- | ------ |
| Numan                  | 10                  |8|
| Simon                  | 10                 |13|
| Donald                 | 06                 |12|
| Davinderpal            | 10                 |15|
| HeSong                 | 10                 |10|


### Phase 4
| Member    | Estimated | Actual |
| ----------- | --------- | ------ |
| Numan                  |7|8|
| Simon                  |8|8|
| Donald                 |8|8|
| Davinderpal            |7|8|
| HeSong                 |7|10|

# Proposal


## Project Proposal
The Valley of the Drums is a toxic waste site in the town of Brooks in Bullitt County, Kentucky. In 1978, the site was found to have a whopping 100,000 toxic waste drums by the Kentucky Department of Natural Resources and Environmental Protection. The site was referenced in the congress hearings that lead to the creation of the Superfund act, which was passed in 1980, and sent aid to allow toxic waste sites such as The Valley of the Drums to be cleaned up. Cleanup of the site took place from 1983 to 1990, but the effects haven’t been so easily cleaned up. Kentucky, due to events like this and high rates of poverty, has the highest cancer rate per capita in the country, yet it lacks the robust cancer treatment facilities that exist in other, richer states. With our project, we hope to provide information about different types of cancer and areas of Kentucky with higher rates of cancer, in an effort to encourage preventative care and early detection. We will also provide information about Kentucky oncologists, so cancer affected Kentuckians will be able to find the care they deserve.

## URLs of at least three data sources that you will programmatically scrape
- [https://www.cancer.gov/types](https://www.cancer.gov/types)
- [https://en.wikipedia.org/api/rest_v1/](https://en.wikipedia.org/api/rest_v1/) (RESTful) (For cancer type information)
- [https://developers.google.com/maps](https://developers.google.com/maps) (RESTful)
- [https://statecancerprofiles.cancer.gov/incidencerates/index](https://statecancerprofiles.cancer.gov/map/map.withimage.php?21&county&001&001&00&0&01&0&1&5&0#results)
- [https://www.cancer-rates.com/ky/](https://www.cancer-rates.com/ky/)
- [https://ukhealthcare.uky.edu](https://ukhealthcare.uky.edu/search#doctors?s=eJyrVkrLTM1JiS9OLSrLTE6Nz8nMS1WyUjI1MjRXqgUAohQJ5Q%3D%3D) (Oncologists)

## Models
- [Types of Cancer Kentucky’s Cancer Center Handles](https://ukhealthcare.uky.edu/markey-cancer-center/cancer-types)
- Oncologists in Kentucky
- Cancer Incidence rates in Kentucky Counties

## An estimate of the number of instances of each model
- Types of cancer: 50
- Oncologists: 150+
- Counties: 120

## Attributes of Each Model
1. Types of Cancer Kentucky’s Cancer Center Handles
- Symptoms
- How common it is
- Who does it affect (by age, gender)
- [Classification](https://en.wikipedia.org/wiki/Cancer#Classification)
- Part of the body/body system
- [Specialty](https://en.wikipedia.org/wiki/Medical_specialty)
- Diagnosis Method
2. Oncologists
- Locations
- Rating
- Specialties
- Related Services
- Conditions they treat
- Faculty Rank
3. Cancer Incidence rates in Kentucky Counties
- Total Population
- Number of Cases
- Crude Cancer Rate
- Age Adjusted rate
- Trend Data over Past 5 Years
- Location

## Instances of each model must connect to instances of at least two other models
- Types of Cancer Kentucky’s Cancer Center Handles
	- Connects to the Oncologists that specialize in treating this type
	- Connects to counties that have higher risk rates for this type
- Oncologists
	- Connects to the types of cancer they treat
	- Connects to the counties they are located in
- Cancer Incidence rates in Kentucky Counties
	- Connects to the types of cancer that have high rates for this county
	- Connects to which oncologists treat this type of cancer

## Describe two types of media for instances of each model
- Types of Cancer Kentucky’s Cancer Center Handles
	- Medical illustration
	- Youtube video giving an explanation
- Oncologists
	- Map of their location(s)
	- Photos of the facilities
	- Photo of the Oncologist
- Cancer Incidence rates in Kentucky Counties
	- Map of where this county is in relation to the Valley of the Drums
	- Trend Graph for the past 5 years

## Describe three questions that your site will answer
1. What types of cancer can be treated at the Markey Cancer Center in Kentucky?
2. What cancer treatment Oncologists are near me?
3. How have cancer rates in Kentucky changed over the past 5 years, and which ones are at high risk?
