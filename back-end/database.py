from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
from sqlalchemy.dialects.postgresql import JSON

pwd = "CS373GROUP17PWD"
app = Flask(__name__)
#app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://"
app.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql://postgres.blilycldjnrhnqswpsng:{pwd}@aws-0-us-west-1.pooler.supabase.com:5432/postgres"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "Herson"

db = SQLAlchemy()
db.init_app(app)


# cancer table (student)
# class Cancer(db.Model):
#     __tablename__ = "Cancer"
#     id = db.Column(db.Integer, primary_key=True)
#     search = db.Column(db.String(255)) 
#     name = db.Column(db.String(64), nullable=False)
#     symptoms = db.Column(JSON)  # Update this line
#     frequency = db.Column(db.Integer)
#     riskFactors = db.Column(JSON)
#     classifications = db.Column(JSON)
#     partOfBody = db.Column(JSON)
#     specialty = db.Column(JSON)
#     diagnosisMethods = db.Column(JSON)
#     description = db.Column(db.Text)
#     image = db.Column(db.String(128))
#     youtube = db.Column(db.String(255))
    
class Cancer(db.Model):
    __tablename__ = "Cancer"
    id = db.Column(db.Integer, primary_key=True)
    search = db.Column(db.String(255)) 
    name = db.Column(db.String(64), nullable=False)
    symptoms = db.Column(JSON)  # Update this line
    frequency = db.Column(db.Integer)
    riskFactors = db.Column(JSON)
    classifications = db.Column(JSON)
    partOfBody = db.Column(JSON)
    specialty = db.Column(JSON)
    diagnosisMethods = db.Column(JSON)
    description = db.Column(db.Text)
    image = db.Column(db.String(128))
    youtube = db.Column(db.String(255))
    prevention = db.Column(JSON)
    relatedCounties = db.Column(JSON)
    relatedOncologists = db.Column(JSON)


class Oncologist(db.Model):
    __tablename__ = "Oncologist"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    image = db.Column(db.String(128))
    conditions = db.Column(JSON)
    location = db.Column(JSON)
    background = db.Column(JSON)
    rating = db.Column(db.String(5), nullable=True)
    reviews = db.Column(JSON)
    relatedCounties = db.Column(JSON)
    relatedTypes = db.Column(JSON)


class County(db.Model):
    __tablename__ = 'County'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    total_population = db.Column(db.String(255))
    number_of_cases = db.Column(db.String(255))
    crude_cancer_rate = db.Column(db.String(255))
    age_adjusted_rate = db.Column(db.String(255))
    description = db.Column(db.Text)
    trend_data = db.Column(db.JSON)
    image = db.Column(db.String(255))
    location = db.Column(db.String(255))
    cancer_rates = db.Column(db.JSON)
    relatedTypes = db.Column(JSON)
    relatedOncologists = db.Column(JSON)
    
if __name__ == '__main__':
    with app.app_context():
        db.drop_all()
        db.create_all()
