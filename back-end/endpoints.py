from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from database import app, db, Cancer, Oncologist, County
import json
from flask_cors import CORS
from DBoperation import Load_Cancer, Load_Oncologist, Load_County
from sqlalchemy import desc, text

api = Api(app)
CORS(app)

# function renderTypes() {
#     let remainingTypes = [];
#     for (let i = 0; i < pageTitles.length; i++) {
#         if (remainingTypes.length >= 4) {
#             break;
#         }
#         let title = pageTitles[i];
#         for (let condition of person.conditions) {
#             if (condition.wikiTitle.toLowerCase().includes(title.wiki.toLowerCase())) {
#                 remainingTypes.push(cancerData[i]);
#             }
#         }
#     }

#     return remainingTypes.map((type, i) => {
#         return <TypeCard key={"type" + i} type={type} rate={0} />;
#     });
# }


class Oncologist_All(Resource):
    def get(self):
        oncologists = Oncologist.query.all()
        ret = []
        for o in oncologists:
            ret.append(
                {
                    "id": o.id,
                    "name": o.name,
                    "image": o.image,
                    "conditions": o.conditions,
                    "location": o.location,
                    "background": o.background,
                    "rating": o.rating or "None",
                    "reviews": o.reviews,
                    "relatedCounties": o.relatedCounties,
                    "relatedTypes": o.relatedTypes,
                }
            )
        return jsonify(ret)


# filter out data for oncologists
class Oncologist_Single(Resource):
    def get(self, oncologist_id):
        oncologist = Oncologist.query.get(oncologist_id)
        if oncologist:
            # If the oncologist is found, return its data as JSON
            return jsonify(
                {
                    "id": oncologist.id,
                    "name": oncologist.name,
                    "image": oncologist.image,
                    "conditions": oncologist.conditions,
                    "location": oncologist.location,
                    "background": oncologist.background,
                    "rating": oncologist.rating or "None",
                    "reviews": oncologist.reviews,
                    "relatedCounties": oncologist.relatedCounties,
                    "relatedTypes": oncologist.relatedTypes,
                }
            )
        else:
            # If oncologist is not found, return a 404 error
            return jsonify({"error": "Oncologist not found"}), 404


class Cancer_Types(Resource):
    def get(self):
        types = Cancer.query.all()
        ret = []
        for t in types:
            ret.append(
                {
                    "name": t.name,
                    "search": t.search,
                    "symptoms": t.symptoms,
                    "frequency": t.frequency,
                    "riskFactors": t.riskFactors,
                    "classifications": t.classifications,
                    "partOfBody": t.partOfBody,
                    "specialty": t.specialty,
                    "diagnosisMethods": t.diagnosisMethods,
                    "description": t.description,
                    "image": t.image,
                    "youtube": t.youtube,
                    "prevention": t.prevention,
                    "relatedCounties": t.relatedCounties,
                    "relatedOncologists": t.relatedOncologists,
                }
            )
        return jsonify(ret)


class Cancer_Type(Resource):
    def get(self, name):
        t = Cancer.query.filter(Cancer.name == name).first()
        if t:
            return jsonify(
                {
                    "name": t.name,
                    "search": t.search,
                    "symptoms": t.symptoms,
                    "frequency": t.frequency,
                    "riskFactors": t.riskFactors,
                    "classifications": t.classifications,
                    "partOfBody": t.partOfBody,
                    "specialty": t.specialty,
                    "diagnosisMethods": t.diagnosisMethods,
                    "description": t.description,
                    "image": t.image,
                    "youtube": t.youtube,
                    "prevention": t.prevention,
                    "relatedCounties": t.relatedCounties,
                    "relatedOncologists": t.relatedOncologists,
                }
            )
        else:
            return jsonify({"error": "Oncologist not found"}), 404


class County_All(Resource):
    def get(self):
        counties = County.query.all()
        ret = []
        for c in counties:
            ret.append(
                {
                    "name": c.name,
                    "totalPopulation": c.total_population,
                    "numberOfCases": c.number_of_cases,
                    "crudeCancerRate": c.crude_cancer_rate,
                    "ageAdjustedRate": c.age_adjusted_rate,
                    "description": c.description,
                    "trendData": c.trend_data,
                    "image": c.image,
                    "location": c.location,
                    "cancer_rates": c.cancer_rates,
                    "relatedTypes": c.relatedTypes,
                    "relatedOncologists": c.relatedOncologists,
                }
            )
        return jsonify(ret)


class County_Single(Resource):
    def get(self, name):
        c = County.query.filter(County.name == name).first()
        if c:
            return jsonify(
                {
                    "name": c.name,
                    "totalPopulation": c.total_population,
                    "numberOfCases": c.number_of_cases,
                    "crudeCancerRate": c.crude_cancer_rate,
                    "ageAdjustedRate": c.age_adjusted_rate,
                    "description": c.description,
                    "trendData": c.trend_data,
                    "image": c.image,
                    "location": c.location,
                    "cancer_rates": c.cancer_rates,
                    "relatedTypes": c.relatedTypes,
                    "relatedOncologists": c.relatedOncologists,
                }
            )
        else:
            return jsonify({"error": "Oncologist not found"}), 404


class Home(Resource):
    def get(self):
        # Load_County()
        return "Hello I Am Here!"


def calculate_relevance(query, document):
    query_words = set(query.lower().split())
    relevance_score = 0
    for field, value in document.items():
        if isinstance(value, str):
            # Calculate relevance score for string fields
            field_words = value.lower().split()
            if query.lower() == value.lower():
                # Exact match of the entire input phrase
                relevance_score += 1000
            elif all(word in field_words for word in query_words):
                # Check if the query phrase is a consecutive subphrase
                query_len = len(query_words)
                field_len = len(field_words)
                for i in range(field_len - query_len + 1):
                    if field_words[i : i + query_len] == query_words:
                        # Increment relevance score for consecutive subphrase match
                        relevance_score += 500
                        break
            else:
                # Single-word matches
                relevance_score += sum(1 for word in query_words if word in field_words)
        elif isinstance(value, dict):
            # Extract values from the dictionary to make a match
            dict_values = " ".join(str(v) for v in value.values())
            relevance_score += calculate_relevance(query, {1: dict_values})
    return relevance_score


def relevance_county(query, document):
    query_words = set(query.lower().split())
    relevance_score = 0
    str_fields = ["name", "description", "location"]
    list_fields = ["relatedTypes"]
    for field in str_fields:
        value = document[field]
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    for field in list_fields:
        value = " ".join(str(v) for v in document[field])
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    return relevance_score


def relevance_Oncologist(query, document):
    query_words = set(query.lower().split())
    relevance_score = 0
    str_fields = ["name"]
    list_fields = ["reviews", "relatedTypes", "relatedCounties"]
    for field in str_fields:
        value = document[field]
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    for field in list_fields:
        value = " ".join(str(v) for v in document[field])
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    value = []
    for dict in document["conditions"]:
        value.append(dict["type"])
        value.append(dict["wikiTitle"])
    for val in document["location"]:
        value.append(val)
    for val in document["background"]:
        value += val
    value = " ".join(str(v) for v in value)
    # Calculate relevance score for string fields
    field_words = value.lower().split()
    if query.lower() == value.lower():
        # Exact match of the entire input phrase
        relevance_score += 1000
    elif all(word in field_words for word in query_words):
        # Check if the query phrase is a consecutive subphrase
        query_len = len(query_words)
        field_len = len(field_words)
        for i in range(field_len - query_len + 1):
            if field_words[i : i + query_len] == query_words:
                # Increment relevance score for consecutive subphrase match
                relevance_score += 500
                break
    # Single-word matches
    relevance_score += sum(1 for word in query_words if word in field_words)
    return relevance_score


def relevance_Type(query, document):
    query_words = set(query.lower().split())
    relevance_score = 0
    str_fields = ["search", "name", "description", "image", "youtube"]
    list_fields = [
        "symptoms",
        "riskFactors",
        "classifications",
        "partOfBody",
        "specialty",
        "diagnosisMethods",
        "prevention",
    ]
    for field in str_fields:
        value = document[field]
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    for field in list_fields:
        value = " ".join(str(v) for v in document[field])
        # Calculate relevance score for string fields
        field_words = value.lower().split()
        if query.lower() == value.lower():
            # Exact match of the entire input phrase
            relevance_score += 1000
        elif all(word in field_words for word in query_words):
            # Check if the query phrase is a consecutive subphrase
            query_len = len(query_words)
            field_len = len(field_words)
            for i in range(field_len - query_len + 1):
                if field_words[i : i + query_len] == query_words:
                    # Increment relevance score for consecutive subphrase match
                    relevance_score += 500
                    break
        # Single-word matches
        relevance_score += sum(1 for word in query_words if word in field_words)
    return relevance_score


class General_searchBar(Resource):
    def post(self):
        data = request.get_json()
        search_query = data.get("searchQuery", "")
        search_results = []

        # Search in Counties
        counties = County.query.all()
        for result in counties:
            relevance_score = relevance_county(
                search_query,
                {
                    "name": result.name,
                    "totalPopulation": result.total_population,
                    "numberOfCases": result.number_of_cases,
                    "crudeCancerRate": result.crude_cancer_rate,
                    "ageAdjustedRate": result.age_adjusted_rate,
                    "description": result.description,
                    "trendData": result.trend_data,
                    "image": result.image,
                    "location": result.location,
                    "cancer_rates": result.cancer_rates,
                    "relatedTypes": result.relatedTypes,
                },
            )
            if relevance_score > 0:
                search_results.append((result, relevance_score))

        # Search in Cancer Types
        cancers = Cancer.query.all()
        for result in cancers:
            relevance_score = relevance_Type(
                search_query,
                {
                    "name": result.name,
                    "search": result.search,
                    "symptoms": result.symptoms,
                    "frequency": result.frequency,
                    "riskFactors": result.riskFactors,
                    "classifications": result.classifications,
                    "partOfBody": result.partOfBody,
                    "specialty": result.specialty,
                    "diagnosisMethods": result.diagnosisMethods,
                    "description": result.description,
                    "image": result.image,
                    "youtube": result.youtube,
                    "prevention": result.prevention,
                    "relatedCounties": result.relatedCounties,
                    "relatedOncologists": result.relatedOncologists,
                },
            )
            if relevance_score > 0:
                search_results.append((result, relevance_score))

        # Search in Oncologists
        ons = Oncologist.query.all()
        for result in ons:
            relevance_score = relevance_Oncologist(
                search_query,
                {
                    "id": result.id,
                    "name": result.name,
                    "image": result.image,
                    "conditions": result.conditions,
                    "location": result.location,
                    "background": result.background,
                    "rating": result.rating,
                    "reviews": result.reviews,
                    "relatedCounties": result.relatedCounties,
                    "relatedTypes": result.relatedTypes,
                },
            )
            if relevance_score > 0:
                search_results.append((result, relevance_score))

        # Sort search results by relevance score
        sorted_results = sorted(search_results, key=lambda x: x[1], reverse=True)

        # Extract relevant information from search results
        formatted_results = []
        for result, score in sorted_results:
            if isinstance(result, County):
                formatted_results.append(
                    {
                        "type": "county",
                        "data": {
                            "name": result.name,
                            "totalPopulation": result.total_population,
                            "numberOfCases": result.number_of_cases,
                            "crudeCancerRate": result.crude_cancer_rate,
                            "ageAdjustedRate": result.age_adjusted_rate,
                            "description": result.description,
                            "trendData": result.trend_data,
                            "image": result.image,
                            "location": result.location,
                            "cancer_rates": result.cancer_rates,
                            "relatedTypes": result.relatedTypes,
                            "relatedOncologists": result.relatedOncologists,
                        },
                    }
                )
            elif isinstance(result, Cancer):
                formatted_results.append(
                    {
                        "type": "cancer",
                        "data": {
                            "name": result.name,
                            "search": result.search,
                            "symptoms": result.symptoms,
                            "frequency": result.frequency,
                            "riskFactors": result.riskFactors,
                            "classifications": result.classifications,
                            "partOfBody": result.partOfBody,
                            "specialty": result.specialty,
                            "diagnosisMethods": result.diagnosisMethods,
                            "description": result.description,
                            "image": result.image,
                            "youtube": result.youtube,
                            "prevention": result.prevention,
                            "relatedCounties": result.relatedCounties,
                            "relatedOncologists": result.relatedOncologists,
                        },
                    }
                )
            elif isinstance(result, Oncologist):
                formatted_results.append(
                    {
                        "type": "oncologist",
                        "data": {
                            "id": result.id,
                            "name": result.name,
                            "image": result.image,
                            "conditions": result.conditions,
                            "location": result.location,
                            "background": result.background,
                            "rating": result.rating,
                            "reviews": result.reviews,
                            "relatedCounties": result.relatedCounties,
                            "relatedTypes": result.relatedTypes,
                        },
                    }
                )

        return jsonify(formatted_results)

def county_search(search_query, query):
    search_results = []
    counties = query.all()

    # Perform search and calculate relevance scores
    for c in counties:
        relevance_score = relevance_county(
            search_query,
            {
                "name": c.name,
                "totalPopulation": c.total_population,
                "numberOfCases": c.number_of_cases,
                "crudeCancerRate": c.crude_cancer_rate,
                "ageAdjustedRate": c.age_adjusted_rate,
                "description": c.description,
                "trendData": c.trend_data,
                "image": c.image,
                "location": c.location,
                "cancer_rates": c.cancer_rates,
                "relatedTypes": c.relatedTypes,
            },
        )
        if relevance_score > 0 or search_query == "":
            search_results.append((c, relevance_score))

    # Sort search results by relevance score
    # sorted_results = sorted(search_results, key=lambda x: x[1], reverse=True)

    # # Extract relevant information from search results
    # formatted_results = [o for o, score in sorted_results]
    # return formatted_results    
    ret = [o for o, score in search_results]
    return ret

def cancer_types_search(search_query, cancers):
    search_results = []
    # cancers = Cancer.query.all()

    # Perform search and calculate relevance scores
    for cancer in cancers:
        relevance_score = relevance_Type(
            search_query,
            {
                "name": cancer.name,
                "search": cancer.search,
                "symptoms": cancer.symptoms,
                "frequency": cancer.frequency,
                "riskFactors": cancer.riskFactors,
                "classifications": cancer.classifications,
                "partOfBody": cancer.partOfBody,
                "specialty": cancer.specialty,
                "diagnosisMethods": cancer.diagnosisMethods,
                "description": cancer.description,
                "image": cancer.image,
                "youtube": cancer.youtube,
                "prevention": cancer.prevention,
                "relatedCounties": cancer.relatedCounties,
                "relatedOncologists": cancer.relatedOncologists,
            },
        )
        if relevance_score > 0 or search_query == "":
            search_results.append((cancer, relevance_score))

    # Sort search results by relevance score
    # sorted_results = sorted(search_results, key=lambda x: x[1], reverse=True)

    # # Extract relevant information from search results
    # formatted_results = [o for o, score in sorted_results]
    # return formatted_results

    ret = [o for o, score in search_results]
    return ret


def oncologist_search(search_query, query):
    # search_query = data.get("searchQuery", "")
    search_results = []
    ons = query.all()

    # Perform search and calculate relevance scores
    for o in ons:
        relevance_score = relevance_Oncologist(
            search_query,
            {
                "id": o.id,
                "name": o.name,
                "image": o.image,
                "conditions": o.conditions,
                "location": o.location,
                "background": o.background,
                "rating": o.rating,
                "reviews": o.reviews,
                "relatedCounties": o.relatedCounties,
                "relatedTypes": o.relatedTypes,
            },
        )
        if relevance_score > 0 or search_query == "":
            search_results.append((o, relevance_score))

    # Sort search results by relevance score
    # sorted_results = sorted(search_results, key=lambda x: x[1], reverse=True)

    # # Extract relevant information from search results
    # formatted_results = [o for o, score in sorted_results]
    # return formatted_results
    ret = [o for o, score in search_results]
    return ret


class County_Search(Resource):
    def post(self):
        data = request.get_json()

         # Sorting

        sortParams = data["sortParams"]
        populationOrder = sortParams.get("population", None)
        casesOrder = sortParams.get("annualCases", None)
        crudeOrder = sortParams.get("crudeRate", None)
        ageOrder = sortParams.get("ageRate", None)

        query = County.query

        if populationOrder == "desc":
            query = query.order_by(desc(text("cast(replace(total_population, ',', '') as Integer)")))
        elif populationOrder == "asc":
            query = query.order_by(text("cast(replace(total_population, ',', '') as Integer)"))

        if casesOrder == "desc":
            query = query.order_by(desc(text("cast(replace(number_of_cases, ',', '') as Integer)")))
        elif populationOrder == "asc":
            query = query.order_by(text("cast(replace(number_of_cases, ',', '') as Integer)"))

        if crudeOrder == "desc":
            query = query.order_by(desc(text("crude_cancer_rate")))
        elif crudeOrder == "asc":
            query = query.order_by(text("crude_cancer_rate"))

        if ageOrder == "desc":
            query = query.order_by(desc(text("age_adjusted_rate")))
        elif ageOrder == "asc":
            query = query.order_by(text("age_adjusted_rate"))

        # Searching
        searchQuery = data["searchQuery"]
        # Filtering
        filterParams = data["filterParams"]

        population = (
            int(filterParams["population"])
            if filterParams.get("population", None)
            else 0
        )
        numCases = (
            int(filterParams["numCases"]) if filterParams.get("numCases", None) else 0
        )
        crudeRate = (
            float(filterParams["crudeRate"])
            if filterParams.get("crudeRate", None)
            else 0.0
        )
        ageRate = (
            float(filterParams["ageRate"]) if filterParams.get("ageRate", None) else 0.0
        )
        cancer = set(
            [item.lower() for item in filterParams["cancer"]]
            if filterParams.get("cancer", None)
            else []
        )
        # counties = County.query.all()
        if searchQuery == "":
            counties = query.all()
        else:
            counties = county_search(searchQuery, query)

        if not counties:
            return []

        ret = []
        for c in counties:

            # if c.name.toLowerCase().include(searchQuery.toLowerCase()) == False:
            #    continue
            # Identifying data that can be filtered on and changing it into a form that can be compared with input parameters
            cancers = sorted(
                c.cancer_rates.keys(),
                key=lambda x: int(c.cancer_rates[x].replace(",", "")),
                reverse=True,
            )

            # Replace "and" because the input cancers have been adjusted to only have "&" instead of "and"
            # topCancers = set([cancers[i].lower().replace('and', '&') for i in range(3)])
            topCancers = set([cancers[i].lower() for i in range(3)])

            # Checking if county meets filter criteria
            if (
                int(c.total_population.replace(",", "")) >= population
                and cancer.issubset(topCancers)
                and int(c.number_of_cases.replace(",", "")) >= numCases
                and float(c.crude_cancer_rate) >= crudeRate
                and float(c.age_adjusted_rate) >= ageRate
            ):
                ret.append(
                    {
                        "name": c.name,
                        "totalPopulation": c.total_population,
                        "numberOfCases": c.number_of_cases,
                        "crudeCancerRate": c.crude_cancer_rate,
                        "ageAdjustedRate": c.age_adjusted_rate,
                        "description": c.description,
                        "trendData": c.trend_data,
                        "image": c.image,
                        "location": c.location,
                        "cancer_rates": c.cancer_rates,
                    }
                )
        return jsonify(ret)


class Cancer_Types_Search(Resource):
    def post(self):

        data = request.get_json()
        # Searching
        searchQuery = data["searchQuery"]

        # Sorting - Only sortable catergory is frequency
        sortParams = data["sortParams"]
        order = sortParams.get("frequency", None)
        query = Cancer.query
        if order is not None:
            if order == "desc":
                query = query.order_by(desc(text("frequency")))
            elif order == "asc" :
                query = query.order_by(text("frequency"))

        # Filtering
        filterParams = data["filterParams"]

        minFrequency = (
            float(filterParams["frequency"])
            if filterParams.get("frequency", None)
            else 0.0
        )
        specialty = set(
            [item.lower() for item in filterParams["specialty"]]
            if filterParams.get("specialty", None)
            else []
        )
        partOfBody = set(
            [item.lower() for item in filterParams["body"]]
            if filterParams.get("body", None)
            else []
        )
        classifications = set(
            [item.lower() for item in filterParams["classifications"]]
            if filterParams.get("classifications", None)
            else []
        )
        symptoms = set(
            [item.lower() for item in filterParams["symptoms"]]
            if filterParams.get("symptoms", None)
            else []
        )
        if searchQuery == "":
            t = query.all()
        else:
            t = query.all()
            t = cancer_types_search(searchQuery, t)
        # t = query.all()
        # t = cancer_types_search(searchQuery, t)
        ret = []
        for cancer in t:
            # if cancer.name.toLowerCase().include(searchQuery.toLowerCase()) == False:
            #    continue
            # Changing all of filterable attributes for a cancer type to a set and making all words lowercase
            partOfBodyAltered = set([item.lower() for item in cancer.partOfBody])
            spcialtyAltered = set([item.lower() for item in cancer.specialty])
            classificationAltered = set(
                [item.lower() for item in cancer.classifications]
            )
            symptomAltered = set([item.lower() for item in cancer.symptoms])

            # Checks if frequency of cancer is less than given value and if it contains all of the other filtered values
            if (
                partOfBody.issubset(partOfBodyAltered)
                and classifications.issubset(classificationAltered)
                and specialty.issubset(spcialtyAltered)
                and symptoms.issubset(symptomAltered)
                and cancer.frequency >= minFrequency
            ):
                ret.append(
                    {
                        "name": cancer.name,
                        "search": cancer.search,
                        "symptoms": cancer.symptoms,
                        "frequency": cancer.frequency,
                        "riskFactors": cancer.riskFactors,
                        "classifications": cancer.classifications,
                        "partOfBody": cancer.partOfBody,
                        "specialty": cancer.specialty,
                        "diagnosisMethods": cancer.diagnosisMethods,
                        "description": cancer.description,
                        "image": cancer.image,
                        "youtube": cancer.youtube,
                        "prevention": cancer.prevention,
                        "relatedCounties": cancer.relatedCounties,
                        "relatedOncologists": cancer.relatedOncologists,
                    }
                )

        return jsonify(ret)


class Oncologist_Search(Resource):
    def post(self):

        data = request.get_json()

        # Searching
        searchQuery = data["searchQuery"]

        # Sorting - only sortable category is rating
        sortParams = data["sortParams"]
        order = sortParams.get("rating", None)
        query = Oncologist.query
        if order is not None:
            if order == "desc":
                query = query.order_by(desc(text("replace(rating, 'None', '0.00')")))
            elif order == "asc":
                #query = query.order_by(text("replace(rating, 'None', 5.01)"))
                query = query.order_by("rating")

        filterParams = data["filterParams"]

        rating = (
            float(filterParams["rating"]) if filterParams.get("rating", None) else 0.0
        )
        conditions = set(
            [item.lower() for item in filterParams["conditions"]]
            if filterParams.get("conditions", None)
            else []
        )
        zipCode = set(
            filterParams["zipCode"] if filterParams.get("zipCode", None) else []
        )
        clinic = set(
            [item.lower() for item in filterParams["clinic"]]
            if filterParams.get("clinic", None)
            else []
        )
        school = set(
            [item.lower() for item in filterParams["school"]]
            if filterParams.get("school", None)
            else []
        )
        if (searchQuery == "") :
            oncologists = query.all()
        else :
            oncologists = oncologist_search(searchQuery, query)
        # oncologists = query.all()

        ret = []
        for o in oncologists:
            # if o.name.toLowerCase().include(searchQuery.toLowerCase()) == False:
            #    continue
            conds = o.conditions
            bg = o.background
            location = o.location

            # Identifying data that can be filtered on and changing it into a form that can be compared with input parameters
            conditionAltered = set([item["type"].lower() for item in conds])
            ratingAltered = float(o.rating) if o.rating != "None" else 0.0
            clinicAltered = location["name"].lower()
            educationAltered = set([item.lower() for item in bg["degree"]])

            splitAddress = location["address"].split(" ")
            zipCodeAltered = splitAddress[len(splitAddress) - 1]

            # Checking if oncologist meets filter criteria
            if (
                ratingAltered >= rating
                and conditions.issubset(conditionAltered)
                and (not clinic or clinicAltered in clinic)
                and (not school or len(school.intersection(educationAltered)) > 0)
                and (not zipCode or zipCodeAltered in zipCode)
            ):
                ret.append(
                    {
                        "id": o.id,
                        "name": o.name,
                        "image": o.image,
                        "conditions": o.conditions,
                        "location": o.location,
                        "background": o.background,
                        "rating": o.rating,
                        "reviews": o.reviews,
                        "relatedCounties": o.relatedCounties,
                        "relatedTypes": o.relatedTypes,
                    }
                )
        return jsonify(ret)


# api.add_resource(TypesFilter, "/types_filter")
# api.add_resource(OncologistFilter, "/oncologists_filter")
# api.add_resource(CountiesFilter, "/counties_filter")

api.add_resource(County_Search, "/counties/search")
api.add_resource(Oncologist_Search, "/oncologist/search")
api.add_resource(Cancer_Types_Search, "/cancer_types/search")
# api.add_resource(County_SearchBar, "/counties/searchbar")
# api.add_resource(Oncologist_SearchBar, "/oncologist/searchbar")
# api.add_resource(Cancer_Types_SearchBar, "/cancer_types/searchbar")
api.add_resource(General_searchBar, "/searchbar")

api.add_resource(Oncologist_All, "/oncologist")
api.add_resource(Oncologist_Single, "/oncologist/<int:oncologist_id>")
api.add_resource(Cancer_Types, "/cancer_types")
api.add_resource(Cancer_Type, "/cancer_types/<string:name>")
api.add_resource(County_All, "/counties")
api.add_resource(County_Single, "/counties/<string:name>")
api.add_resource(Home, "/")

if __name__ == "__main__":
    app.run(port=5000)
