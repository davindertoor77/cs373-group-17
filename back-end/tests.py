import unittest
import json
from endpoints import app


class ApiTests(unittest.TestCase):

    def setUp(self):
        app.config["TESTING"] = True
        self.app = app.test_client()

    def test_home_endpoint(self):
        response = self.app.get("/")
        self.assertEqual(response.status_code, 200)
        data = json.dumps(response.json)
        self.assertEqual(data, '"Hello I Am Here!"')

    def test_oncologists_all(self):
        response = self.app.get("/oncologist")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for oncologist in data:
            self.assertTrue(oncologist["id"] >= 0)
            self.assertTrue(oncologist["background"])
            self.assertTrue(oncologist["location"])
            self.assertTrue(isinstance(oncologist["conditions"], list))

    def test_oncologist(self):
        response = self.app.get("/oncologist/1")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        self.assertTrue(data["background"])
        self.assertTrue(isinstance(data["conditions"], list))
        self.assertTrue(data["name"])
        self.assertTrue(data["rating"])
        self.assertTrue(data["location"])

    def test_oncologist_search(self):
        dictToSend = {
            "searchQuery": "Thyroid",
            "filterParams": {
                "rating": 3,
                "conditions": ["Adrenal cancer"],
                "zipCode": ["40536"],
                "clinic": ["Head, Neck & Respiratory Clinic"],
                "school": [
                    "The Warren Alpert Medical School of Brown University, Providence, R.I."
                ],
            },
            "sortParams": {"rating": "asc"},
        }
        response = self.app.post("/oncologist/search", json=dictToSend)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for oncologist in data:
            self.assertTrue(oncologist["id"] >= 0)
            self.assertTrue(oncologist["background"])
            self.assertTrue(oncologist["location"])
            self.assertTrue(isinstance(oncologist["conditions"], list))

    def test_county_all(self):
        response = self.app.get("/counties")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for county in data:
            self.assertTrue(county["name"])
            self.assertTrue(county["crudeCancerRate"])
            self.assertTrue(county["trendData"])
            self.assertTrue(county["location"])
            self.assertTrue(county["cancer_rates"])
            self.assertTrue(isinstance(county["crudeCancerRate"], str))

    def test_county(self):
        response = self.app.get("/counties/Jefferson")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        self.assertTrue(data["location"])
        self.assertTrue(data["cancer_rates"])

    def test_county_search(self):
        dictToSend = {
            "searchQuery": "Floyd",
            "filterParams": {
                "population": 100,
                "numCases": 100,
                "crudeRate": 820.5,
                "ageRate": 600,
                "cancer": ["lung and bronchus"],
            },
            "sortParams": {
                "population": "asc",
                "annualCases": "desc",
                "crudeRate": "asc",
                "ageRate": "desc",
            },
        }
        response = self.app.post("/counties/search", json=dictToSend)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for county in data:
            self.assertTrue(county["name"])
            self.assertTrue(county["crudeCancerRate"])
            self.assertTrue(county["trendData"])
            self.assertTrue(county["location"])
            self.assertTrue(county["cancer_rates"])
            self.assertTrue(isinstance(county["crudeCancerRate"], str))

    def test_cancer_types(self):
        response = self.app.get("/cancer_types")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for cancer in data:
            self.assertTrue(cancer["name"])
            self.assertTrue(cancer["symptoms"])
            self.assertTrue(cancer["frequency"])
            self.assertTrue(isinstance(cancer["classifications"], list))
            self.assertTrue(cancer["riskFactors"])

    def test_cancer_type(self):
        response = self.app.get("/cancer_types/Lung_cancer")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        self.assertTrue(data["name"])
        self.assertTrue(data["symptoms"])
        self.assertTrue(data["frequency"])
        self.assertTrue(isinstance(data["classifications"], list))
        self.assertTrue(data["riskFactors"])

    def test_cancer_search(self):
        dictToSend = {
            "searchQuery": "Lung_cancer",
            "filterParams": {
                "classifications": ["small cell"],
                "symptoms": ["chest pain"],
                "body": ["lungs"],
                "specialty": ["oncology"],
                "frequency": 500,
            },
            "sortParams": {"frequency": "asc"},
        }
        response = self.app.post("/cancer_types/search", json=dictToSend)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for cancer in data:
            self.assertTrue(cancer["name"])
            self.assertTrue(cancer["symptoms"])
            self.assertTrue(cancer["frequency"])
            self.assertTrue(isinstance(cancer["classifications"], list))
            self.assertTrue(cancer["riskFactors"])
            
    def test_global_search(self):
        dictToSend = {
            "searchQuery": "thyroid"
        }
        response = self.app.post("/searchbar", json=dictToSend)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json)
        data = response.json
        for entry in data:
            self.assertTrue(entry["data"])
            self.assertTrue(entry["type"])


if __name__ == "__main__":
    unittest.main()
