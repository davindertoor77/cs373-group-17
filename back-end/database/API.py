from flask import Flask, jsonify
from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from database import app, db, Cancer, Oncologist, County
import json

api = Api(app)

    # function renderTypes() {
    #     let remainingTypes = [];
    #     for (let i = 0; i < pageTitles.length; i++) {
    #         if (remainingTypes.length >= 4) {
    #             break;
    #         }
    #         let title = pageTitles[i];
    #         for (let condition of person.conditions) {
    #             if (condition.wikiTitle.toLowerCase().includes(title.wiki.toLowerCase())) {
    #                 remainingTypes.push(cancerData[i]);
    #             }
    #         }
    #     }

    #     return remainingTypes.map((type, i) => {
    #         return <TypeCard key={"type" + i} type={type} rate={0} />;
    #     });
    # }

class Oncologist_All(Resource):
    def get(self):
        oncologists = Oncologist.query.all()
        ret = []
        for o in oncologists:
            ret.append({
                "id": o.id,
                "name": o.name,
                "image": o.image,
                "conditions": o.conditions,
                "location": o.location,
                "background": o.background,
                "rating": o.rating or "None",
                "reviews": o.reviews
            })
        return jsonify(ret)

# filter out data for oncologists
class Oncologist_Single(Resource):
    def get(self, oncologist_id):
        oncologist = Oncologist.query.get(oncologist_id + 1)
        if oncologist:
            # If the oncologist is found, return its data as JSON
            return jsonify({
                "id": oncologist.id,
                "name": oncologist.name,
                "image": oncologist.image,
                "conditions": oncologist.conditions,
                "location": oncologist.location,
                "background": oncologist.background,
                "rating": oncologist.rating or "None",
                "reviews": oncologist.reviews
            })
        else:
            # If oncologist is not found, return a 404 error
            return jsonify({"error": "Oncologist not found"}), 404

class Cancer_Types(Resource):
    def get(self):
        types = Cancer.query.all()
        ret = []
        for t in types:
            ret.append({
                "name": t.name,
                "search": t.search,
                "symptoms": t.symptoms,
                "frequency": t.frequency,
                "riskFactors": t.riskFactors,
                "classifications": t.classifications,
                "partOfBody": t.partOfBody,
                "specialty": t.specialty,
                "diagnosisMethods": t.diagnosisMethods,
                "description": t.description,
                "image": t.image,
                "youtube": t.youtube
            })
        return jsonify(ret)

class Cancer_Type(Resource):
    def get(self, name):
        t = Cancer.query.filter(Cancer.name == name).first()
        if t:
            return jsonify({
                "name": t.name,
                "search": t.search,
                "symptoms": t.symptoms,
                "frequency": t.frequency,
                "riskFactors": t.riskFactors,
                "classifications": t.classifications,
                "partOfBody": t.partOfBody,
                "specialty": t.specialty,
                "diagnosisMethods": t.diagnosisMethods,
                "description": t.description,
                "image": t.image,
                "youtube": t.youtube
            })
        else:
            return jsonify({"error": "Oncologist not found"}), 404

class County_All(Resource):
    def get(self):
        counties = County.query.all()
        ret = []
        for c in counties:
            ret.append({
                "name": c.name,
                "totalPopulation": c.total_population,
                "numberOfCases": c.number_of_cases,
                "crudeCancerRate": c.crude_cancer_rate,
                "ageAdjustedRate": c.age_adjusted_rate,
                "description": c.description,
                "trendData": c.trend_data,
                "image": c.image,
                "location": c.location,
                "nearbyTreatmentCenters": c.nearby_treatment_centers,
                "cancer_rates": c.cancer_rates
            })
        return jsonify(ret)

class County_Single(Resource):
    def get(self, name):
        c = County.query.filter(County.name == name).first()
        if c:
            return jsonify({
                "name": c.name,
                "totalPopulation": c.total_population,
                "numberOfCases": c.number_of_cases,
                "crudeCancerRate": c.crude_cancer_rate,
                "ageAdjustedRate": c.age_adjusted_rate,
                "description": c.description,
                "trendData": c.trend_data,
                "image": c.image,
                "location": c.location,
                "nearbyTreatmentCenters": c.nearby_treatment_centers,
                "cancer_rates": c.cancer_rates
            })
        else:
            return jsonify({"error": "Oncologist not found"}), 404

class Home(Resource):
    def get(self):
        return "Hello I Am Here!"

api.add_resource(Oncologist_All, "/oncologist")
api.add_resource(Oncologist_Single, "/oncologist/<int:oncologist_id>")
api.add_resource(Cancer_Types, "/cancer_types")
api.add_resource(Cancer_Type, "/cancer_types/<string:name>")
api.add_resource(County_All, "/counties")
api.add_resource(County_Single, "/counties/<string:name>")
api.add_resource(Home, "/")

if __name__ == "__main__":
    app.run(port=5000, debug=True)
