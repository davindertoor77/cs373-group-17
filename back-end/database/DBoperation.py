from database import app, db, Cancer, Oncologist, County
import json
def Load_Cancer():
    with open('../scraper/out/typeData.json', "r", encoding = 'utf-8') as file:
        data = json.load(file)
        for entry in data:
            name = entry['name']
            search = entry['search']
            symptoms = entry['symptoms']
            frequency = entry['frequency']
            riskFactors = entry['riskFactors']
            classifications = entry['classifications']
            partOfBody = entry['partOfBody']
            specialty = entry['specialty']
            diagnosisMethods = entry['diagnosisMethods']
            description = entry['description']
            image = entry['image']
            youtube = entry['youtube']

            cancer = Cancer(name=name, search=search, symptoms=symptoms, frequency=frequency,
                            riskFactors=riskFactors, classifications=classifications, partOfBody=partOfBody,
                            specialty=specialty, diagnosisMethods=diagnosisMethods, description=description,
                            image=image, youtube=youtube)
            # Add instance to session and commit changes
            with app.app_context():
                db.session.add(cancer)
                db.session.commit()
def Load_Oncologist():
    with open('../scraper/out/oncologistData.json', 'r', encoding = 'utf-8') as file:
        data = json.load(file)
        for oncologist_data in data:
            oncologist = Oncologist(
                name=oncologist_data['name'],
                image=oncologist_data['image'],
                conditions=json.dumps(oncologist_data['conditions']),
                location=json.dumps(oncologist_data['location']),
                background=json.dumps(oncologist_data['background']),
                rating=oncologist_data['rating'],
                reviews=json.dumps(oncologist_data['reviews'])
            )
            with app.app_context():
                db.session.add(oncologist)
                db.session.commit()

def Load_County():
    with open('../scraper/out/countyData.json', "r", encoding = 'utf-8') as file:
        county_data = json.load(file)

    for county_info in county_data:
        county = County(
            name=county_info['name'],
            total_population=county_info['totalPopulation'],
            number_of_cases=county_info['numberOfCases'],
            crude_cancer_rate=county_info['crudeCancerRate'],
            age_adjusted_rate=county_info['ageAdjustedRate'],
            description=county_info['description'],
            trend_data=county_info['trendData'],
            image=county_info['image'],
            location=county_info['location'],
            cancer_rates=county_info['cancerRates']
        )
        with app.app_context():
            # Add the new record to the database session
            db.session.add(county)
            # Commit the transaction
            db.session.commit()

def insert_cancer(name, symptoms, frequency, riskFactors, classifications, partOfBody, specialty, diagnosisMethods, description, image, youtube):
    new_cancer = Cancer(name=name, symptoms=symptoms, frequency=frequency, riskFactors=riskFactors,
                        classifications=classifications, partOfBody=partOfBody, specialty=specialty,
                        diagnosisMethods=diagnosisMethods, description=description, image=image, youtube=youtube)
    db.session.add(new_cancer)
    db.session.commit()
    return new_cancer

# Delete method for Cancer by name
def delete_cancer_by_name(name):
    cancers = Cancer.query.filter_by(name=name).all()
    for cancer in cancers:
        db.session.delete(cancer)
    db.session.commit()

# Filter method for Cancer by frequency greater than a given value and sorted in ascending order
def filter_cancer_by_frequency_greater_than_ordered(fre):
    cancers = Cancer.query.filter(Cancer.frequency > fre).order_by(Cancer.frequency.asc()).all()
    return [cancer.__dict__ for cancer in cancers]

def reset_sequence(table_name):
    db.session.execute(db.text(f"CREATE SEQUENCE IF NOT EXISTS {table_name}_id_seq START 1"))
    db.session.execute(db.text(f"ALTER SEQUENCE {table_name}_id_seq RESTART WITH 1"))


def clear_database():
    # Delete all records from the Cancer table
    Cancer.query.delete()
    # Delete all records from the Oncologist table
    Oncologist.query.delete()
    # Delete all records from the County table
    County.query.delete()

    # Reset sequence for each table
    reset_sequence("Cancer")
    reset_sequence("Oncologist")
    reset_sequence("County")
    
    # Commit the changes to the database
    db.session.commit()

if __name__ == "__main__":
    print("setting up database")
    with app.app_context():
        try:
            print("clearing database")
            clear_database()
        except Exception as e:
            print(f"Error occurred while clearing the database: {e}")
            # Rollback the transaction in case of any errors
            db.session.rollback()
        else:
            print("loading database")
            Load_Cancer()
            Load_County()
            Load_Oncologist()
            
            # try:
            #     print("Resetting sequence for tables")
            #     db.session.execute(db.text("ALTER SEQUENCE Cancer_id_seq RESTART WITH 1;"))
            #     db.session.execute(db.text("ALTER SEQUENCE Oncologist_id_seq RESTART WITH 1;"))
            #     db.session.execute(db.text("ALTER SEQUENCE County_id_seq RESTART WITH 1;"))
            #     db.session.commit()
            # except Exception as e:
            #     print(f"Error occurred while resetting sequence: {e}")
            #     db.session.rollback()