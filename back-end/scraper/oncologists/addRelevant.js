'use strict';
import { typeData, countyData, oncologistData, pageTitles } from "./types.cjs";
import fs from "fs";

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
    let currentIndex = array.length;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {

        // Pick a remaining element...
        let randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
}

let out = [];
let index = 0;
for (let oncologist of oncologistData) {
    let relevantCounties = [];
    let relevantTypes = [];
    for (let i = 0; i < pageTitles.length; i++) {
        if (relevantTypes.length >= 4) {
            break;
        }
        let title = pageTitles[i];
        for (let condition of oncologist.conditions) {
            if (condition.wikiTitle.toLowerCase().includes(title.wiki.toLowerCase())) {
                relevantTypes.push(typeData[i].name);
            }
        }
    }

    shuffle(countyData);
    relevantCounties = countyData.slice(0, 4).map(x => x.name);

    oncologist.relatedTypes = relevantTypes;
    oncologist.relatedCounties = relevantCounties;
    out.push(oncologist);
    index++;
}

fs.writeFileSync("../out/oncologistDataFinal.json", JSON.stringify(out));