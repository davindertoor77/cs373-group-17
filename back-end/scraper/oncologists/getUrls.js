import puppeteer from 'puppeteer';
import fs from "fs";

async function autoScroll(page) {
  await page.evaluate(async () => {
    await new Promise((resolve) => {
      var totalHeight = 0;
      var distance = 100;
      var timer = setInterval(() => {
        var scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if (totalHeight >= scrollHeight - window.innerHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 100);
    });
  });
}

(async () => {
  let data = await fetch("https://ukhealthcare.uky.edu/api/search?sort=field_last_name%2Cfield_first_name&field_service_line=5217&type=profile&limit=160");
  let json = await data.json();
  // fs.writeFileSync("../out/oncologistURLs.json", JSON.stringify(json));
  console.log("File Made");
  console.log(json.data.length);
  let urls = [];
  for (let i = 0; i < json.data.length; i++) {
    urls.push({
      "url": json.data[i].url,
      "firstName": json.data[i].field_first_name,
      "lastName": json.data[i].field_last_name,
      "image": json.data[i].field_thumbnail_url || "/themes/custom/ukhc/images/Profile-Image-01.png"
    });
    // let imageURL = json.data[i].field_thumbnail_url || "/themes/custom/ukhc/images/Profile-Image-01.png";

    // let imageSRC = `https://ukhealthcare.uky.edu/${imageURL.replace("styles/profile_search_result/public/", "styles/large/public/")}`;
    // let imgFetch = await fetch(imageSRC);
    // let imgBuffer = Buffer.from(await imgFetch.arrayBuffer());
    // fs.writeFileSync(`../out/oncologists/${i}.png`, imgBuffer, { flag: "w+" });
    // data.image = `oncologists/${data.id}.png`;

  }
  fs.writeFileSync("../out/oncologistURLs.json", JSON.stringify(urls));
})();

/**
 * {
      "search_id": "entity:node/10416:en",
      "excerpt": "Thyroid Cancer Team Medical oncology Thyroid & parathyroid cancers Adrenal cancer Professor of Medicine The Warren Alpert Medical School of Brown University, Providence, R.I. Hahnemann University Hospital, Philadelphia National Institute of Health, Bethesda, Md. University of Chicago Hospital and Clinics, Chicago American Board of Internal Medicine American Board of Internal Medicine, …",
      "field_first_name": "Kenneth",
      "field_gender": "Male",
      "field_last_name": "Ain",
      "field_npi_number": "1396857173",
      "field_practice_area": [
        9597
      ],
      "field_profile_type": [
        "Physician",
        "Researcher"
      ],
      "field_rating_score": 4.69,
      "field_service_line": [
        5217
      ],
      "field_service_locations": [
        8604
      ],
      "field_thumbnail_alt": "Kenneth B. Ain, MD",
      "field_thumbnail_url": "/sites/default/files/styles/profile_search_result/public/ain_kennethb_017.jpg",
      "title": "Kenneth B. Ain, MD",
      "type": "profile",
      "url": "/researchers-doctors/kenneth-ain"
    }
 */