import puppeteer from 'puppeteer';
import pageTitles from "../pageTitles.js";
import oncologistURLs from "./package.cjs";
import fs from "fs";

const CONDITIONS_SELECTOR = ".views-field, .views-field-custom-conditions, .views-row-item";
const LOCATIONS_DIV_SELECTOR = ".block-views-blockprofiles-block-profile-detail-locations";
const TRAINING_SELECTOR = ".view-display-id-block_profile_detail_education";

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}

(async () => {
    // Launch the browser and open a new blank page
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    // page.on('console', async (msg) => {
    //     const msgArgs = msg.args();
    //     for (let i = 0; i < msgArgs.length; ++i) {
    //         console.log(await msgArgs[i].jsonValue());
    //     }
    // });

    await page.setViewport({ width: 1080, height: 1024 });
    let finalData = [];
    // Navigate the page to a URL
    for (let urlIndex = 0; urlIndex < 22; urlIndex++) {
        const urlData = oncologistURLs[urlIndex];
        /**
         * Conditions: {
         *      type: "",
         *      wikiTitle: ""
         * }
         */
        let data = {
            id: Number(urlIndex).toString(),
            name: "",
            image: "",
            conditions: [{ "type": "", "wikiTitle": "" }],
            location: {
                name: "",
                address: ""
            },
            background: {
                degree: [],
                residency: [],
                fellowship: [],
                certificates: [],
            },
            rating: "",
            reviews: []
        };
        // We have the reference, push it first for error handling
        finalData.push(data);
        console.log(urlData, data.id);

        // let oncTitle = url.oncologist;
        try {
            await page.goto(`https://ukhealthcare.uky.edu/${urlData.url}`);
            await page.waitForSelector(CONDITIONS_SELECTOR);

            data.name = await page.$eval(".profile-detail__header--main", el => {
                return el.querySelector("h2").innerText;
            });
            //`${urlData.firstName} ${urlData.lastName}`;

            let imageSRC = `https://ukhealthcare.uky.edu/${urlData.image}`;
            let imgFetch = await fetch(imageSRC);
            let imgBuffer = Buffer.from(await imgFetch.arrayBuffer());
            fs.writeFileSync(`../out/oncologists/${data.id}.png`, imgBuffer, { flag: "w+" });
            data.image = `oncologists/${data.id}.png`;

            data.conditions = await page.$$eval(CONDITIONS_SELECTOR, (elList, pageTitles) => {
                let conditions = [];
                for (let i = 0; i < elList.length; i++) {
                    let el = elList[i];
                    if (el.textContent.includes("Conditions")) {
                        let list = el.querySelectorAll("li a");
                        for (let link of list) {
                            let fullURL = "https://ukhealthcare.uky.edu" + link.getAttribute("href");
                            for (let title of pageTitles) {
                                // Store the URL as the Wiki Title for searching purposes
                                if (fullURL.includes(title.oncologist)) {
                                    conditions.push({
                                        "type": link.innerText,
                                        "wikiTitle": title.wiki
                                    });
                                }
                            }
                        }
                        break;
                    }
                }
                return conditions;
            }, pageTitles);

            try {
                data.location = await page.$eval(LOCATIONS_DIV_SELECTOR, el => {
                    let location = {
                        name: "",
                        address: ""
                    };
                    if (el.querySelector(".field") == null) {
                        location.name = el.querySelector(".avenir-demi").innerText;
                    } else {
                        location.name = el.querySelector(".field").innerText;
                    }
                    location.address = el.querySelector(".card-summary__address").innerText;
                    return location;
                });
            } catch (e) {
                // Special Case like Jessica L. Burris, PhD
                data.location = {
                    name: "N/A",
                    address: "N/A"
                };
            }

            data.background = await page.$eval(TRAINING_SELECTOR, mainDiv => {
                let background = {
                    degree: [],
                    residency: [],
                    fellowship: [],
                    certificates: [],
                };
                let children = mainDiv.children;
                for (let i = 0; i < children.length; i++) {
                    let child = children[i];
                    // Structure is H3 then Div of Text (Multiple Possible)
                    if (child.tagName === "H3" && child.textContent.includes("Degree")) {
                        for (let j = i + 1; j < children.length; j++) {
                            let previewChild = children[j];
                            if (previewChild.tagName === "H3") {
                                break;
                            }
                            background.degree.push(previewChild.innerText);
                            i = j;
                        }
                    }
                    if (child.tagName === "H3" && child.textContent.includes("Residency")) {
                        for (let j = i + 1; j < children.length; j++) {
                            let previewChild = children[j];
                            if (previewChild.tagName === "H3") {
                                break;
                            }
                            background.residency.push(previewChild.innerText);
                            i = j;
                        }
                    }
                    if (child.tagName === "H3" && child.textContent.includes("Fellowship")) {
                        for (let j = i + 1; j < children.length; j++) {
                            let previewChild = children[j];
                            if (previewChild.tagName === "H3") {
                                break;
                            }
                            background.fellowship.push(previewChild.innerText);
                            i = j;
                        }
                    }
                    if (child.tagName === "H3" && child.textContent.includes("Certifications")) {
                        for (let j = i + 1; j < children.length; j++) {
                            let previewChild = children[j];
                            if (previewChild.tagName === "H3") {
                                break;
                            }
                            background.certificates.push(previewChild.innerText);
                            i = j;
                        }
                    }
                }
                return background;
            });

            try {
                data.rating = await page.$eval(".rating__text", el => {
                    let text = el.innerText;
                    return text.substring(0, text.indexOf(" "));
                });
            } catch (e) {
                // Special Case like Falguni Amin-Zimmerman, MD
                data.rating = "None";
            }

            try {
                data.reviews = await page.$$eval(".doctors__comment", elList => {
                    let reviews = [];
                    for (let review of elList) {
                        reviews.push(review.querySelector(".comment__text").innerText.trim());
                    }
                    return reviews;
                });
            } catch (e) {
                // Special Case
                data.reviews = [];
            }
        } catch (e) {
            console.log(e);
            break;
        }
        // console.log(JSON.stringify(data));
        // break;
    }
    await browser.close();
    fs.writeFileSync("../out/oncologistData.json", JSON.stringify(finalData));
})();