import puppeteer from 'puppeteer';
import countyData from "./package.cjs";
import fs from "fs";
import fetch from "node-fetch";
import "dotenv/config";

async function getData() {
    const finalData = [];
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    page.on('console', async (msg) => {
        const msgArgs = msg.args();
        for (let i = 0; i < msgArgs.length; ++i) {
            console.log(await msgArgs[i].jsonValue());
        }
    });
    await page.setViewport({ width: 1080, height: 1024 });

    for (let county of countyData) {
        console.log(county.name);
        finalData.push(county);
        try {
            await page.goto(`https://simple.wikipedia.org/wiki/${county.name}_County,_Kentucky`);
            await page.waitForNetworkIdle();
            county.description = await page.$eval(".mw-content-ltr > p:not(.mw-empty-elt)", el => {
                return el.innerText.replace(/\[\d+\]/g, "");
            });

            let imageSrc = await page.$eval(".mw-file-element", el => {
                let src = "https:" + el.getAttribute("src");
                src = src.replace("/thumb", "");
                src = src.replace(/\.jpg.*/, ".jpg");
                src = src.replace(/\.png.*/, ".png");
                src = src.replace(/\.JPG.*/, ".JPG");
                return src;
            });
            console.log(imageSrc);
            let imgFetch = await fetch(imageSrc);
            let imgBuffer = Buffer.from(await imgFetch.arrayBuffer());
            fs.writeFileSync(`../out/counties/${county.name}.png`, imgBuffer, { flag: "w+" });
            county.image = `counties/${county.name}.png`;    
        } catch (e) {
            console.log(e);
            county.description = "N/A";
            county.image = "N/A";
        }
    }
    await browser.close()
    fs.writeFileSync("../out/finalCountyData.json", JSON.stringify(finalData));
}

getData();