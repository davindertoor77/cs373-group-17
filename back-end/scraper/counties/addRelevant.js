'use strict';
import { typeData, countyData, oncologistData, pageTitles } from "./package.cjs";
import fs from "fs";

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
    let currentIndex = array.length;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {

        // Pick a remaining element...
        let randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
}

let out = [];
let index = 0;
for (let county of countyData) {
    let relevantOncologists = new Set();
    let classifications = Object.keys(county.cancerRates);

    let remainingTypes = typeData.filter((type) => {
        if (classifications.includes(type.search)) {
            return true;
        }
        return false;
    });
    remainingTypes = remainingTypes
        .sort((a, b) => {
            return county.cancerRates[b.search] - county.cancerRates[a.search];
        });

    // console.log(remainingTypes.map(x => x.name).filter((_, i) => i < 4));
    shuffle(oncologistData);

    for (let type of remainingTypes.map(x => x.name).filter((_, i) => i < 4)) {
        let s = type;
        let breakFlag = false;
        for (let doctor of oncologistData) {
            if (breakFlag) {
                break;
            }
            for (let condition of doctor.conditions) {
                if (
                    condition.wikiTitle.toLowerCase().includes(s.toLowerCase()) ||
                    condition.type.includes("All cancers")
                ) {
                    // console.log(s);
                    if (!relevantOncologists.has(doctor.id)) {
                        relevantOncologists.add(doctor.id);
                        breakFlag = true;
                        break;
                    }
                }
            }
        }
    }

    county.relatedTypes = remainingTypes.map(x => x.name).filter((_, i) => i < 4);
    county.relatedOncologists = [...relevantOncologists].filter((_, i) => i < 4);
    out.push(county);
    index++;
    // break;
}

fs.writeFileSync("../out/countyDataFinal.json", JSON.stringify(out));