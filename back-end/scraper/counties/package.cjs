const typeData = require('../out/typeData.json');
const countyData = require('../out/countyData.json');
const oncologistData = require('../out/oncologistData.json');
const pageTitles = require("../pageTitles.json");
module.exports = { typeData, countyData, oncologistData, pageTitles };