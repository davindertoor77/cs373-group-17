import puppeteer from 'puppeteer';
import fs from "fs";

const POPULATION_SELECTOR = "body > div.modal.modal1.region > div.rates-by-year-container > div.rates-by-year-table > div.viewer.scroll.rates-by-year > table > tbody > tr:nth-child(1) > td:nth-child(6)";
const TOTAL_SELECTOR = "body > div.modal.modal1.region > div.rates-by-year-container > div.rates-by-year-table > div.viewer.scroll.rates-by-year > table > tbody > tr:nth-child(2) > td:nth-child(7)";
const CRUDE_SELECTOR = "body > div.modal.modal1.region > div.rates-by-year-container > div.rates-by-year-table > div.viewer.scroll.rates-by-year > table > tbody > tr:nth-child(3) > td:nth-child(7)";
const AGE_ADJUST_SELECTOR = "body > div.modal.modal1.region > div.rates-by-year-container > div.rates-by-year-table > div.viewer.scroll.rates-by-year > table > tbody > tr:nth-child(4) > td:nth-child(7)";
const TREND_ROW_SELECTOR = "body > div.modal.modal1.region > div.rates-by-year-container > div.rates-by-year-table > div.viewer.scroll.rates-by-year > table > tbody > tr:nth-child(4)";
const FIVE_YEAR_SELECTOR = "body > div.modal.modal1.region > div.tabbar > ul > li.profile";

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}

(async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    page.on('console', async (msg) => {
        const msgArgs = msg.args();
        for (let i = 0; i < msgArgs.length; ++i) {
            console.log(await msgArgs[i].jsonValue());
        }
    });
    await page.setViewport({ width: 1080, height: 1024 });

    await page.goto("https://www.cancer-rates.com/ky/");
    await page.waitForNetworkIdle();
    await delay(2000);

    const titleArr = await page.$$(".region > a, .cell > a");
    // console.log(titleArr);
    const finalData = [];

    try {
        for (let i = 6; i < titleArr.length - 1; i++) {
            let title = titleArr[i];
            let data = {
                name: "",
                totalPopulation: "",
                numberOfCases: "",
                crudeCancerRate: "",
                ageAdjustedRate: "",
                description: "",
                trendData: [],
                image: "",
                location: "",
                //NearbyTreatmentCenters: [], // Might Remove
                cancerRates: {}
            };
            finalData.push(data);
            await title.evaluate(title => {
                title.click();
            });
            // await title.click();
            await delay(2000);
            await page.waitForSelector(".heading, .tall");
            data.name = await page.$eval(".heading, .tall", el => {
                let h1 = el.querySelector("h1");
                return h1.innerText.split(" ")[0];
            });
            console.log(data.name, i);

            data.totalPopulation = await page.$eval(POPULATION_SELECTOR, el => {
                let num = Number(el.innerText);
                return num.toLocaleString();
            });

            data.numberOfCases = await page.$eval(TOTAL_SELECTOR, el => {
                let num = Number(el.innerText);
                return num.toLocaleString();
            });

            data.crudeCancerRate = await page.$eval(CRUDE_SELECTOR, el => {
                let num = Number(el.innerText);
                return num.toLocaleString();
            });

            data.ageAdjustedRate = await page.$eval(AGE_ADJUST_SELECTOR, el => {
                let num = Number(el.innerText);
                return num.toLocaleString();
            });

            data.trendData = await page.$eval(TREND_ROW_SELECTOR, row => {
                let out = [];
                let tds = row.querySelectorAll("td");
                for(let i = 1; i < tds.length - 1; i++) {
                    out.push(Number(tds[i].innerText));
                }
                return out;
            });

            data.location = `${data.name} County, KY`;

            let profileSelector = await page.$(FIVE_YEAR_SELECTOR);
            await profileSelector.click();
            await delay(2000);

            data.cancerRates = await page.$eval(".tblSites", table => {
                let rows = table.querySelectorAll("tr");
                let outRates = {};
                for(let i = 2; i < rows.length; i++) {
                    let childNodes = rows[i].childNodes;
                    let cancerName = childNodes[0].innerText.trim();
                    if(childNodes[2].innerText == "" || childNodes[2].innerText == "0") {

                    } else {
                        let cancerTotal = Number(childNodes[2].innerText.trim()).toLocaleString();
                        outRates[cancerName] = cancerTotal;
                    }
                }
                return outRates;
            });

            let byYearButton = await page.$(".byyear");
            await byYearButton.click();

            let closeButton = (await page.$$(".icon-close"))[2];
            await closeButton.click();
            await delay(1000);
            // break;
        }
    } catch (e) {
        console.log(e);
    }

    await browser.close();
    fs.writeFileSync("../out/countyData.json", JSON.stringify(finalData));
})();
