import fetch from "node-fetch";
import "dotenv/config"
import { parse } from 'node-html-parser';
import { wordsToNumbers } from 'words-to-numbers';
import prompt from "prompt";
import pageTitles from "../pageTitles.js";
import fs from "fs";
import cancerData from "./package.cjs";
// const cancerData = require("../out/puppeteerData.json");

/**
 *  Name: str
    Search: str
    Symptoms: list[str]
    Frequency (global): int or str
    Who does it effect/Risk Factors: list[str]
    Classification: list[str]
    Part of the body: list[str]
    (Medical) Specialty: list[str]
    Diagnosis Methods: list[str]
    Description: str
    Image: str (path to file relative to images/)
    Youtube: str (link to youtube video for embed)
 */
let finalData = [];
prompt.start();

/**
 * Essentially the main() function
 */
async function getData() {
    for (let i = 0; i < pageTitles.length; i++) {
        let data = cancerData[i];
        let wikiTitle = pageTitles[i].wiki;
        data.name = wikiTitle;
        data.search = pageTitles[i].county;
        const url = `https://en.wikipedia.org/w/api.php?action=parse&page=${wikiTitle}&format=json`;

        let response = await fetch(url, {
            headers: {
                "Authorization": process.env.ACCESS_TOKEN,
                "Api-User-Agent": "Bluegrass"
            }
        });
        let json = await response.json();

        let text = json.parse.text["*"];
        const root = parse(text);
        const trows = root.querySelectorAll("tr");
        for (const tr of trows) {
            let th = tr.querySelector("th");
            if (th) {
                if (th.innerText == "Frequency") {
                    data.frequency = await getFrequency(tr);
                    console.log(data.frequency);
                }
            }
        }
        const img = root.querySelector("img");
        const src = "https:" + img.getAttribute("src");
        if (src) {
            // console.log(src);
            let imgFetch = await fetch(src);
            let imgBuffer = Buffer.from(await imgFetch.arrayBuffer());
            fs.writeFileSync(`../out/types/${wikiTitle}.png`, imgBuffer, { flag: "w+" });
            data.image = `types/${wikiTitle}.png`;
        }
        finalData.push(data);
        // break;
    }
    fs.writeFileSync("../out/typeData.json", JSON.stringify(finalData));
}

/**
 * Main function for parsing Frequency Data
 * Pass in the table ROW with the relevant data
 */
async function getFrequency(tr) {
    let data = tr.querySelector("td");
    let text = data.innerText.toString().replace(/\&[^\;]*\;/g, " ");
    let endIndex = text.indexOf("(");
    if (endIndex == -1) {
        endIndex = text.length - 1;
    }
    text = text.substring(0, endIndex);
    text = text.replace(/,/g, "");
    console.log(text);

    let finalFreq = cleverNumberParse(text);
    if (isNaN(finalFreq)) {
        console.log("Enter c to use default regex number parse.");
        let userInput = await userDiscretion(text);
        if (userInput === "c") {
            finalFreq = regexNumberParse(text);
        } else {
            finalFreq = cleverNumberParse(userInput);
            if (isNaN(finalFreq)) {
                finalFreq = userInput;
            }
        }
    }
    return finalFreq;
}

/**
 * Splits frequency by " " and then uses wordsToNumbers to
 * extract values like million into Numbers.
 * @param {String} text 
 * @returns The Total Frequency as a Number (may be NaN)
 */
function cleverNumberParse(text) {
    let arrText = text.split(" ");
    arrText = arrText.map(x => wordsToNumbers(x));
    let finalFreq = 1;
    for (let val of arrText) {
        if (val.length == 0) {
            continue;
        }
        finalFreq *= Number(val);
    }
    return finalFreq;
}

/**
 * Require specific input from the programmer to read the text and return
 * the correct number to display for frequency.
 * @param {String} text 
 * @returns Number for the frequency in the text
 */
async function userDiscretion(text) {
    return new Promise((res, rej) => {
        prompt.get([text], (err, result) => {
            res(result[text]);
        });
    });
}

/**
 * Uses a simple RegEx script to extract numbers the first
 * straight line of numbers.
 * @param {String} text 
 * @returns Number for frequency
 */
function regexNumberParse(text) {
    let res = /\d+/.exec(text);
    return Number(res[0]);
}

getData();