import puppeteer from 'puppeteer';
import pageTitles from "../pageTitles.js";
import fs from "fs";

const DESC_SELECTOR = "#block-ukhc-content > div > article > div > div > div > div.block.block-layout-builder.block-field-blocknodeconditionfield-components > div > div > div.paragraph__component--html-content > div > div";
const SYMPTOM_SELECTOR = ".field--paragraph--field-html";
// const RISK_SELECTOR = "#collapse-42736 > div > div > div > div > div > div > ul";
// const DIAGNOSIS_SELECTOR = "#collapse-61096 > div > div > div > div > div > div > ul";
// const PREVENTION_SELECTOR = "#collapse-42731 > div > div > div > div > div > div > ul";
function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}

(async () => {
    // Launch the browser and open a new blank page
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    // page.on('console', async (msg) => {
    //     const msgArgs = msg.args();
    //     for (let i = 0; i < msgArgs.length; ++i) {
    //         console.log(await msgArgs[i].jsonValue());
    //     }
    // });

    await page.setViewport({ width: 1080, height: 1024 });
    let finalData = [];
    // Navigate the page to a URL
    // pageTitles.forEach(async title => {
    for (let titleIndex = 0; titleIndex < pageTitles.length; titleIndex++) {
        // for (const title of pageTitles) {
        const title = pageTitles[titleIndex];
        let data = {
            "name": "",
            "search": "",
            "symptoms": [],
            "frequency": 0,
            "riskFactors": [],
            "classifications": [],
            "partOfBody": [],
            "specialty": [],
            "diagnosisMethods": [],
            "description": "",
            "image": "",
            "youtube": "",
            "prevention": []
        };
        // We have the reference, push it first for error handling
        finalData.push(data);
        data.name = title.wiki;
        data.search = title.county;
        console.log(data.name, titleIndex);

        let oncTitle = title.oncologist;
        try {
            if (oncTitle !== "N/A") {
                if (oncTitle.indexOf("http") == 0) {
                    // Special Case if weird URL
                    await page.goto(oncTitle);
                } else {
                    await page.goto(`https://ukhealthcare.uky.edu/markey-cancer-center/cancer-types/${oncTitle}`);
                }
                // await page.waitForNetworkIdle();
                await page.waitForSelector(DESC_SELECTOR);

                data.description = await page.$eval(DESC_SELECTOR, el => {
                    let children = el.children;
                    let outText = [];
                    for (let i = 0; i < children.length; i++) {
                        let child = children[i];
                        if (child.nodeName === "UL") {
                            let listNodes = child.children;
                            let tempText = [];
                            for (let j = 0; j < listNodes.length; j++) {
                                tempText.push("\t" + listNodes[j].innerText);
                            }
                            outText.push(tempText.join(""));
                        } else {
                            outText.push(child.innerText);
                        }
                    }
                    return outText.join("\n");
                });

                data.symptoms = await page.$$eval(SYMPTOM_SELECTOR, elList => {
                    const el = elList[1].querySelector("ul");
                    let listElems = el.children;
                    let outArr = [];
                    for (let i = 0; i < listElems.length; i++) {
                        outArr.push(listElems[i].innerText);
                    }
                    return outArr;
                });

                data.riskFactors = await page.$$eval(SYMPTOM_SELECTOR, elList => {
                    let tabTitles = document.querySelectorAll(".ukhc-tabs .nav-item");
                    let riskIndex = -1;
                    for (let i = 0; i < tabTitles.length; i++) {
                        let el = tabTitles[i];
                        if (el.innerText == "Risk factors") {
                            riskIndex = i;
                            break;
                        }
                    }

                    const el = elList[riskIndex + 1].querySelector("ul");
                    let outArr = [];
                    if (el) {
                        let listElems = el.children;
                        for (let i = 0; i < listElems.length; i++) {
                            let listElem = listElems[i];
                            if (listElem.childNodes.length == 1) {
                                // Base Case (breast-cancer)
                                outArr.push(listElem.innerText);
                            } else {
                                // Embolden Text
                                let text = listElem.childNodes[0].innerText.replace(".", ":") + listElem.childNodes[1].textContent;
                                outArr.push(text);
                            }
                        }
                    } else {
                        // Special Case
                        outArr.push(elList[riskIndex + 1].innerText);
                    }
                    return outArr;
                });

                data.prevention = await page.$$eval(SYMPTOM_SELECTOR, elList => {
                    let tabTitles = document.querySelectorAll(".ukhc-tabs .nav-item");
                    let preventionIndex = -1;
                    for (let i = 0; i < tabTitles.length; i++) {
                        let el = tabTitles[i];
                        if (el.innerText == "Prevention") {
                            preventionIndex = i;
                            break;
                        }
                    }
                    const el = elList[preventionIndex + 1].querySelector("ul");
                    let outArr = [];
                    if (el) {
                        let listElems = el.children;
                        for (let i = 0; i < listElems.length; i++) {
                            let listElem = listElems[i];
                            if (listElem.childNodes.length == 1) {
                                // Base Case (penile-cancer)
                                outArr.push(listElem.innerText);
                            } else {
                                // Embolden Text
                                let text = listElem.childNodes[0].innerText.replace(".", ":") + listElem.childNodes[1].textContent;
                                outArr.push(text);
                            }
                        }
                    } else {
                        // Special case like leukemia
                        outArr.push(elList[preventionIndex + 1].innerText);
                    }
                    return outArr;
                });

                // Diagnosis
                if (title.diagnosis) {
                    // Special Case
                    await page.goto(title.diagnosis);
                } else {
                    await page.goto(`https://ukhealthcare.uky.edu/markey-cancer-center/cancer-types/${oncTitle}/diagnosis`);
                }
                await page.waitForSelector(".ukhc-tabs .nav-item");
                console.log("diagnosis time");

                const imagingTestIndex = await page.$$eval(".ukhc-tabs .nav-item", elList => {
                    for (let i = 0; i < elList.length; i++) {
                        let el = elList[i];
                        if (el.innerText == "Imaging tests") {
                            return i;
                        }
                    }
                    return -1;
                });

                if (imagingTestIndex != -1) {
                    data.diagnosisMethods = await page.$$eval(SYMPTOM_SELECTOR, (elList, imagingTestIndex) => {
                        // console.log("IMAGING TEST INDEX", imagingTestIndex);
                        // +1 since the 0th index is not relevant
                        const el = elList[imagingTestIndex + 1].querySelector("ul");
                        if (el) {
                            let listElems = el.children;
                            let outArr = [];
                            for (let i = 0; i < listElems.length; i++) {
                                let listElem = listElems[i];
                                // console.log("Diagnosis Children: ", listElem.childNodes.length);
                                try {
                                    if (listElem.childNodes.length == 2) {
                                        // Embolden Text
                                        let text = listElem.childNodes[0].innerText.replace(".", ":") + listElem.childNodes[1].textContent;
                                        outArr.push(text);
                                    } else if (listElem.childNodes.length == 4) {
                                        // Special case like Pancreas
                                        let text = listElem.childNodes[0].innerText.replace(".", ":") + listElem.childNodes[1].textContent + listElem.childNodes[3].textContent;
                                        outArr.push(text);
                                    } else {
                                        // Href like melanoma
                                        let text = listElem.childNodes[1].innerText.replace(".", ":") + listElem.childNodes[6].textContent;
                                        outArr.push(text);
                                    }
                                } catch (e) {
                                    // I give up tbh
                                    outArr.push(listElem.innerText.replace(".", ":"));
                                }
                            }
                            return outArr;
                        } else {
                            // Not a List, like for prostate cancer
                            let childNodes = elList[2].querySelector("p").childNodes;
                            if (childNodes.length == 1) {
                                // Penile-Cancer special case
                                return [childNodes[0].textContent];
                            } else {
                                return [childNodes[0].innerText.replace(".", ":") + childNodes[1].textContent]
                            }
                        }
                    }, imagingTestIndex);
                } else {
                    console.log("NO DIAGNOSIS FOUND");
                }
            }
            // Specialty
            console.log("specialty time");
            await page.goto(`https://en.wikipedia.org/wiki/${title.wiki}`);
            // await page.waitForNetworkIdle();
            await page.waitForSelector("tr");
            data.specialty = await page.$$eval("tr", trArr => {
                for (let i = 0; i < trArr.length; i++) {
                    let tr = trArr[i];
                    if (tr.innerText.toLowerCase().includes("specialty")) {
                        const specialtyList = tr.querySelector("td").innerText.split(",").map(x => x.trim().toLowerCase());
                        return specialtyList;
                    }
                }
                return [];
            });
        } catch (e) {
            console.log(e);
            break;
        }
        // console.log(JSON.stringify(data));
        // break;
    }
    await browser.close();
    fs.writeFileSync("../out/puppeteerData.json", JSON.stringify(finalData));
})();