import { typeData, countyData, oncologistData, pageTitles } from "./types.cjs";
import fs from "fs";

let out = [];
let index = 0;
for (let type of typeData) {
    let relevantCounties = [];
    let relevantOncologists = [];
    for (let doctor of oncologistData) {
        if (relevantOncologists.length >= 4) {
            break;
        }
        for (let condition of doctor.conditions) {
            if (condition.wikiTitle.toLowerCase().includes(pageTitles[index].wiki.toLowerCase())) {
                relevantOncologists.push(doctor.id);
                break;
            }
        }
    }
    if (relevantOncologists.length == 0) {
        relevantOncologists.push("1");
    }

    // Counties
    relevantCounties = countyData
        .sort((a, b) => b.cancerRates[type.search] - a.cancerRates[type.search])
        .filter((_, i) => i < 4)
        .map(x => x.name);

    type.relatedOncologists = relevantOncologists;
    type.relatedCounties = relevantCounties;
    out.push(type);
    index++;
}

fs.writeFileSync("./out/typesFinal.json", JSON.stringify(out));