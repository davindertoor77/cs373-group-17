import React from "react";
function NoPage() {
    return <h1>Error 404, This Page Does Not Exist!</h1>;
}
export default NoPage;
