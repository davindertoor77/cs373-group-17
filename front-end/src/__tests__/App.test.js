import { render, screen, cleanup, act, waitFor } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";

import Home from "../pages/Home";
import About from "../pages/About";
import Counties from "../pages/Counties";
import Oncologists from "../pages/Oncologists";
import NoPage from "../pages/NoPage";
import Types from "../pages/Types";

afterEach(() => {
  cleanup();
});

test("renders home page", () => {
  render(
    <BrowserRouter>
      <Home />
    </BrowserRouter>
  );
  const header = screen.getByRole("heading", {
    name: /navigating cancer in bluegrass territory/i,
  });
  expect(header).toBeInTheDocument();
});

test("renders about page", async () => {
  await act(async () =>
    render(
      <BrowserRouter>
        <About />
      </BrowserRouter>
    )
  );
  const header = screen.getByRole("heading", {
    name: /about bluegrass cancer navigation/i,
  });
  expect(header).toBeInTheDocument();
});

test("renders cancer types page", async () => {
  await act(async () =>
    render(
      <BrowserRouter>
        <Types />
      </BrowserRouter>
    )
  );
  await new Promise((r) => setTimeout(r, 3000));
  const header = screen.getByRole("heading", {
    name: /types of cancer/i,
  });
  expect(header).toBeInTheDocument();
});

test("renders oncologists page", async () => {
  await act(async () =>
    render(
      <BrowserRouter>
        <Oncologists />
      </BrowserRouter>
    )
  );
  await new Promise((r) => setTimeout(r, 3000));
  const header = screen.getByRole("heading", {
    name: /oncologists in kentucky/i,
  });
  expect(header).toBeInTheDocument();
});

test("renders counties page", async () => {
  await act(async () =>
    render(
      <BrowserRouter>
        <Counties />
      </BrowserRouter>
    )
  );
  await new Promise((r) => setTimeout(r, 3000));
  const header = screen.getByRole("heading", { name: /counties in kentucky/i });
  expect(header).toBeInTheDocument();
});

test("renders 404 page", () => {
  render(
    <BrowserRouter>
      <NoPage />
    </BrowserRouter>
  );
  const header = screen.getByRole("heading", {
    name: /error 404, this page does not exist!/i,
  });
  expect(header).toBeInTheDocument();
});
