const { By, Builder } = require("selenium-webdriver");
const assert = require("assert");
const chrome = require('selenium-webdriver/chrome');

const screen = {
  width: 900,
  height: 640
};

describe("selenium - website renders", () => {

  it("homepage should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/");
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Navigating Cancer in Bluegrass Territory", title);
    if (driver) await driver.quit();
  }, 60_000);

  it("visualizations should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/visualizations");
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Visualizations", title);
    if (driver) await driver.quit();
  });

  it("provider visualizations should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/provider_visualizations");
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Provider Visualizations", title);
    if (driver) await driver.quit();
  });

  it("global search should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/search/a");
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Search", title);
    if (driver) await driver.quit();
  });

  it("about should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/about");
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("About Bluegrass Cancer Navigation", title);
    if (driver) await driver.quit();
  });
  
  it("types of cancers should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/types");
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Types of Cancer", title);
    if (driver) await driver.quit();
  });

  it("oncologists should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/oncologists");
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Oncologists in Kentucky", title);
    if (driver) await driver.quit();
  });

  it("counties should load", async function () {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/counties");
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Counties in Kentucky", title);
    if (driver) await driver.quit();
  });
});

describe("selenium - links redirect correctly", () => {
  it("types should redirect to correct instance page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/types");    
    await driver.manage().setTimeouts({ implicit: 5000 });
    const instanceCard = await driver.findElement(By.xpath("//a[text()='More Info']/.."))
    let itemName = await instanceCard.findElement(By.tagName("div")).getText()
    const button = await instanceCard.findElement(By.tagName("a"))

    // Click button and go to next page. Confirm page is correct
    await driver.executeScript("arguments[0].click();", button)
    await driver.manage().setTimeouts({ implicit: 5000 });
    let title = await driver.getTitle()
    title = title.split("_")
    title = title.join(" ")
    title = title.toLowerCase()
    itemName = title.toLowerCase()
    
    assert.equal(title, "lung cancer")

    if (driver) await driver.quit();
  })

  it("counties should redirect to correct instance page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/counties"); 
    await driver.manage().setTimeouts({ implicit: 2000 });
    const instanceCard = await driver.findElement(By.xpath("//a[text()='More Info']/.."))
    const itemName = await instanceCard.findElement(By.tagName("div")).getText()
    const button = await instanceCard.findElement(By.tagName("a"))

    // Click button and go to next page. Confirm page is correct
    await driver.executeScript("arguments[0].click();", button)
    await driver.manage().setTimeouts({ implicit: 2000 });
    const title = await driver.getTitle()
    assert.equal(title, "Floyd")

    if (driver) await driver.quit();
  })

  it("oncologists should redirect to correct instance page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/oncologists");    
    await driver.manage().setTimeouts({ implicit: 2000 });
    const instanceCard = await driver.findElement(By.xpath("//a[text()='More Info']/.."))
    const itemName = await instanceCard.findElement(By.tagName("div")).getText()
    const button = await instanceCard.findElement(By.tagName("a"))

    // Click button and go to next page. Confirm page is correct
    await driver.executeScript("arguments[0].click();", button)
    await driver.manage().setTimeouts({ implicit: 2000 });
    // Grab oncologist page header (their name)
    const title = await driver.findElement(By.tagName('p')).getText()
    assert.equal(title, "Kenneth B. Ain, MD")

    if (driver) await driver.quit();
  })
})

describe("selenium - home page buttons link correctly", () => {
  it("home page correctly redirects to types page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/");  
    const button = await driver.findElement(By.xpath("//button[text()='Cancer Types']"))
    await button.click()
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Types of Cancer", title);
    if (driver) await driver.quit();
  }) 

  it("home page correctly redirects to oncologists page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/");  
    const button = await driver.findElement(By.xpath("//button[text()='Oncologists']"))
    await button.click()
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Oncologists in Kentucky", title);
    if (driver) await driver.quit();
  }) 

  it("home page correctly redirects to counties page", async () => {
    const driver = await new Builder().forBrowser("chrome").setChromeOptions(new chrome.Options().addArguments('--headless', '--no-sandbox').windowSize(screen)).build();
    await driver.get("https://www.bluegrasscancernav.me/");  
    const button = await driver.findElement(By.xpath("//button[text()='KY Counties']"))
    await button.click()
    await driver.manage().setTimeouts({ implicit: 5000 });
    const title = await driver.findElement(By.tagName("h1")).getText()
    assert.equal("Counties in Kentucky", title);
    if (driver) await driver.quit();
  }) 
})