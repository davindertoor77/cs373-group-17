import { render, screen, cleanup, act } from "@testing-library/react";
import Member from "../components/about/Member";

const member = {
  name: "Ned",
  alt_name: "ned123",
  user_id: 12345678,
  image: "members/numan-headshot.jpg",
  commits: 7,
  tests: 0,
  issues: 7,
  email: "ned@utexas.edu",
  bio: "Ned here",
  role: "Fullstack",
};

beforeEach(async () => {
  await act(async() => render(
    <Member
      name={member.name}
      imageSrc={member.image}
      commits={member.commits || 0}
      issues={member.issues || 0}
      email={member.email}
      bio={member.bio}
      role={member.role}
    />
  ))
})

afterEach(() => {
  cleanup();
});

it("should render Member component", () => {
  const memberElem = screen.getByTestId("member");
  expect(memberElem).toBeInTheDocument();
});

it("should have correct name", () => {
  const elem = screen.getByTestId("name");
  expect(elem.textContent).toBe(member.name)
});

it("should have correct role", () => {
  const elem = screen.getByTestId("role");
  expect(elem.textContent).toBe(member.role)
});

it("should have correct bio", () => {
  const elem = screen.getByTestId("bio");
  expect(elem.textContent).toBe(member.bio)
});

it("should have correct email", () => {
  const elem = screen.getByTestId("email");
  expect(elem.textContent).toBe(member.email)
});

it("should have correct issues", async () => {
  const elem = await screen.findByTestId("issues");
  expect(elem.textContent).toBe(`Issues Closed: ${member.issues}`)
});

it("should have correct commits", async () => {
  expect(await screen.findByText(`Commits: ${member.commits}`)).toBeInTheDocument()
})
