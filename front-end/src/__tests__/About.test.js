import { render, screen, cleanup, act } from "@testing-library/react";
import mockAboutFetch from "../mocks/mockFetch";
import About from "../pages/About";

test("should properly fetch and set commits, issues", async () => {
  jest.spyOn(global, "fetch").mockImplementation(mockAboutFetch);

  await act(async () => {
    render(<About />);
  }); 
  expect(screen.getByText("Total Commits: 25Total Issues: 50")).toBeInTheDocument();
  
}); 
