import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import PropTypes from "prop-types";
import useImage from "../utils/useImage";
import { dummyOncologists } from "../utils/dummyData";
import cancerData from "../utils/cancerData.json";
import { countyData } from "../utils/countyData";

const InstanceCard = ({ id, model }) => {
    //Link to instance page i.e. types/Sarcoma
    const link = model + "/" + id;

    //Will hold data about the instance to be represented
    var instance;

    //Variables that will be populated with relevant data for this id/model
    //Declared as such so the code for an instance card can be generic
    var attr1;
    var attr2;
    var attr3;
    var attr4;
    var attr5;

    //Used for styling purpose
    //Will be popuated differently depending on the model
    var attr1name;
    var attr2name;
    var attr3name;
    var attr4name;
    var attr5name;

    //Will be populated with location of image for given id/model
    var imageAddr;

    //Depending on the type of model, different behavior
    if (model == "oncologists") {
        instance = dummyOncologists.find((o) => o.id === id);
        attr1name = "Rating: ";
        attr1 = instance.rating;
        attr2name = "Specialties: ";
        attr2 = "";
        //Get all conditions. Last index handled differently because it
        //shouldn't have a comma at the end. All following loops behave the same
        for (let i = 0; i < instance.conditions.length - 1; i++) {
            attr2 += instance.conditions[i].type + ", ";
        }
        attr2 += instance.conditions[instance.conditions.length - 1].type;
        attr3name = "Clinic: ";
        attr3 = instance.location.name;
        attr4name = "Address: ";
        attr4 = instance.location.address;
        attr5name = "Education: ";
        attr5 = instance.background.degree;
        imageAddr = "oncologists/oncologist" + id + ".jpg";
    } else if (model == "type") {
        instance = cancerData.find((o) => o.name === id);
        attr1name = "Classification: ";
        attr1 = "";
        //Get all classifications
        for (let i = 0; i < instance.classifications.length - 1; i++) {
            attr1 += instance.classifications[i] + ", ";
        }
        attr1 += instance.classifications[instance.classifications.length - 1];
        attr2name = "Frequency: ";
        attr2 = instance.frequency.toLocaleString();
        attr3name = "Common Symptoms: ";
        attr3 = "";
        //Get all conditions
        for (let i = 0; i < instance.symptoms.length - 1; i++) {
            attr3 += instance.symptoms[i] + ", ";
        }
        attr3 += instance.symptoms[instance.symptoms.length - 1];
        attr4name = "Part of body: ";
        attr4 = "";
        //get all partsOfBody
        for (let i = 0; i < instance.partOfBody.length - 1; i++) {
            attr4 += instance.partOfBody[i] + ", ";
        }
        attr4 += instance.partOfBody[instance.partOfBody.length - 1];
        attr5name = "Specialty: ";
        attr5 = "";
        //get all specialties
        for (let i = 0; i < instance.specialty.length - 1; i++) {
            attr5 += instance.specialty[i] + ", ";
        }
        attr5 += instance.specialty[instance.specialty.length - 1];
        imageAddr = instance.image;
    } else if (model == "county") {
        instance = countyData.find((o) => o.name === id);
        attr1name = "Population: ";
        attr1 = instance.totalPopulation.toLocaleString();
        attr2name = "Annual cases: ";
        attr2 = instance.numberOfCases.toLocaleString();
        attr3name = "Crude Cancer Rate: ";
        attr3 = instance.crudeCancerRate;
        attr4name = "Age adjusted rate: ";
        attr4 = instance.ageAdjustedRate;
        attr5name = "";
        attr5 = "";
        imageAddr = "counties/" + id + ".png";
    }
    const { image } = useImage(imageAddr);
    return (
        <Card className="modelCard">
            <Card.Img
                variant="top"
                src={image}
                height={"auto"}
                style={{ objectFit: "stretch", aspectRatio: "1" }}
                className="instanceCardImage"
            />
            <Card.Body>
                <Card.Title>{instance.name}</Card.Title>
                <Card.Text>
                    {/*Attributes will be accordingly populated above*/}
                    <strong>{attr1name}</strong>
                    {attr1}
                    <br />
                    <strong>{attr2name}</strong>
                    {attr2}
                    <br />
                    <strong>{attr3name}</strong>
                    {attr3}
                    <br />
                    <strong>{attr4name}</strong>
                    {attr4}
                    <br />
                    <strong>{attr5name}</strong>
                    {attr5}
                </Card.Text>
                <Button href={link}>More Info</Button>
            </Card.Body>
        </Card>
    );
};

InstanceCard.propTypes = {
    id: PropTypes.string,
    model: PropTypes.string
};
export default InstanceCard;
