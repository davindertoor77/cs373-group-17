/* eslint-disable react/prop-types */
import React from 'react'

export const SearchList = ({ results }) => {
    return (
        <div>
            {results.map((result, id) => {
            return <div key={id}>{result}</div>;
        })}
        </div>
    );
};
