const fakeData = [
  {
    name: "Numan Osagie",
    commits: 5,
  },
  {
    name: "Simon Stone",
    commits: 5,
  },
  {
    name: "Donald Thai",
    commits: 5,
  },
  {
    name: "Davinderpal Toor",
    commits: 5,
  },
  {
    name: "HeSong Wang",
    commits: 5,
  },
];

export default async function mockAboutFetch(url) {
  switch (url) {
    case "https://gitlab.com/api/v4/projects/54609874/repository/contributors": {
      return {
        json: async () => fakeData,
      };
    }
    default: {
      const dummy = {
        statistics: {
          counts: {
            closed: 5,
            all: 10,
          },
        },
      };
      return {
        json: async () => dummy,
      };
    }
  }
}
