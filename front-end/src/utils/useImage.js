// Source: https://stackoverflow.com/a/70024111
import { useEffect, useState } from 'react'

/**
 * Function to dynamically load images using the {fileName}
 * @param {String} fileName String path relative to the images folder.
 * @returns The image will be stored in the image section of the returned block.
 */
const useImage = (fileName) => {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [image, setImage] = useState(null)

    useEffect(() => {
        const fetchImage = async () => {
            try {
                const response = await import(`../images/${fileName}`) // change relative path to suit your needs
                setImage(response.default)
            } catch (err) {
                setError(err)
            } finally {
                setLoading(false)
            }
        }

        fetchImage()
    }, [fileName])

    return {
        loading,
        error,
        image,
    }
}

export default useImage