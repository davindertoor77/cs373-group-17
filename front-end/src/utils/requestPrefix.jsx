let requestPrefix;

if(process.env.NODE_ENV == "development") {
    requestPrefix = "http://127.0.0.1:5000";
} else {
    requestPrefix = "https://cs373-group-17.onrender.com/";
}

export default requestPrefix;