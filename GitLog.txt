commit ddcdb153cb40b179ef17b6d8ddba579af8d0a509
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 23:38:57 2024 -0600

    Finish README, Update About, Update Model Counts

commit 4c47737fa49abd7f4aea1f1b737a8d4c32b907b9
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 23:21:26 2024 -0600

    Git Log, close #48

commit 1c12ee974bc66d4c97e1309c35b989099816c0c2
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 23:20:22 2024 -0600

    Report, close #60

commit ee5de92b0aca16c77c3351a975f1796d73a5c0ea
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 22:56:17 2024 -0600

    Fix Navbar, Add More Routing for Instances

commit 529c349b6e60702d620334e8ffd694391fffbc3e
Merge: 18661b9 b9579c9
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 21:34:00 2024 -0600

    Merged

commit 18661b9ddbf76f38840d48a161f5f44782baa524
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 21:32:29 2024 -0600

    Temporary merge

commit b9579c939f8f7bfcee1431966dded29c57826dbe
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Thu Feb 15 01:46:12 2024 +0000

    Update bio and role info

commit cf29805463df7fdfe62d91c30269fe3c5526acbd
Merge: da3da0d 4b6dbc2
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Thu Feb 15 01:12:58 2024 +0000

    Merge branch 'main' into 'main'
    
    County instance page responsiveness implemented
    
    See merge request davindertoor77/cs373-group-17!5

commit da3da0dab2f3ef58eb0a17eff0c129a6b025b19b
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Thu Feb 15 01:10:47 2024 +0000

    numan about information

commit 4b6dbc2c9c26cc83435967198fc16f825a50b8e3
Merge: c272f3b cd8ab9e
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 19:03:32 2024 -0600

    Merge branch 'main' of https://gitlab.com/Hersoncpp/cs373-group-17

commit c272f3b34c87f7ff21f07dbb2821ce4936c797ec
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 18:50:29 2024 -0600

    county instance page responsiveness implemented

commit cd8ab9e131174be4dc1adb40d28543a08ff3e52b
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Wed Feb 14 17:56:22 2024 -0600

    Types page adjustment

commit 90a095c0f100072ef1afb270ca7e9d597e77f270
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Wed Feb 14 17:52:53 2024 -0600

    Oops

commit 4a6301358b90ba7f010c1b901ead8c7a56cf68cf
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Wed Feb 14 17:49:30 2024 -0600

    Added some comments, slight Types fix

commit 351b38f8a88f6d7845c054e753296debdbcc4205
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Wed Feb 14 17:46:42 2024 -0600

    Added Simon email lol

commit a0359a4b43afef094be3045d1883bf085ad11283
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Wed Feb 14 17:45:33 2024 -0600

    Added Simon info

commit 69c142545abca4ad69c6224b19f8b3af88ee8665
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Wed Feb 14 17:20:08 2024 -0600

    Added about information and fixed styling

commit e139d3c25e1fc371e8a5fe9443b25ff36faeff3b
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 14:54:37 2024 -0600

    pipeline

commit de35d9364ed61fa4886ffa14db202a0dd8ab50e1
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 14:50:53 2024 -0600

    Margin Left + YAML, close #74

commit 82b25de834dca0364f20aeebef0ac8f88dfffb6a
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 14:16:15 2024 -0600

    Image URLs, Number Formatting

commit 1285cead2dcda429142eb2291901243cc9bfd139
Merge: 323681c 3f8e8ef
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 14:04:33 2024 -0600

    Merge remote-tracking branch 'refs/remotes/origin/main'

commit 323681c8b579575e7d1027d142583e673930f8ae
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 14:04:31 2024 -0600

    Routing to COunties from Types

commit 3f8e8effb0bf125e5951d76e258aeb46ed91303b
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Wed Feb 14 13:44:10 2024 -0600

    Updated Instance Card

commit 7b5542e9735512388a89c05c380038943a081608
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 13:17:42 2024 -0600

    Add row/cols for model pages, close #75

commit 49cb246a2a5b858c1910bb355c2c247be3669fae
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 12:55:13 2024 -0600

    Close #76

commit 591f1f0a971d0004e1fc1c0103651d574af00025
Merge: 7361ef3 d5979cb
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 12:47:39 2024 -0600

    Merge branch 'main' of gitlab.com:davindertoor77/cs373-group-17

commit 7361ef35b265c9e5e0705c632b98d2c9a27800e4
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Wed Feb 14 12:47:29 2024 -0600

    Temp Map and Fix Nav, close #77

commit d5979cb7ad0c743d9f824a8f18d5feae741b08b4
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Wed Feb 14 12:46:56 2024 -0600

    Updated counties model page

commit acc785cb4221e313863e04160c8d186a5f7702ff
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 22:59:45 2024 -0600

    Added cancer types model page again?

commit dc218581eef05f373266236ceb608d43d2dc08e1
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Wed Feb 14 04:55:34 2024 +0000

    added oncologist placeholder map

commit 476b4829f5f27a2591488643bf55f87f08292cae
Merge: 2024d49 ea7cfc6
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 22:51:39 2024 -0600

    Added types of cancer model page

commit 2024d4923138c77a38f22b71ab396444cdd52e01
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 22:49:26 2024 -0600

    Added cancer types model page

commit ea7cfc69553ce8167c356f2bc9379bb05d328298
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 22:12:29 2024 -0600

    Yaml Update

commit fc5b12b75e4c82a25b7043d83d98dcaa0863e244
Merge: 0cbf509 48b81ac
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 03:58:23 2024 +0000

    Merge branch 'main' into 'main'
    
    county instance page done
    
    See merge request davindertoor77/cs373-group-17!4

commit 48b81accc3aeb07a2bcbe97eb8f4034fcc34e7e2
Merge: c930ceb 0cbf509
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 03:57:00 2024 +0000

    Merge branch cs373-group-17:main into main

commit 0cbf5093c005c030e7bd8331ac4bba873158e3a9
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 21:54:52 2024 -0600

    Added Instance Card

commit c930ceb4dc21bef5493507b9752595ffeba25845
Merge: cbb2424 3637121
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 03:51:53 2024 +0000

    Merge branch cs373-group-17:main into main

commit 36371217daa128ccac6f460250e1d7c63ae14e80
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 21:50:32 2024 -0600

    Added model page for oncologists

commit cbb2424d24de567e48def442f1fe7bcef5a0a075
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 21:49:59 2024 -0600

    ac

commit caebbc3d50d9332d0307ac2333c60d443f2be20a
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 21:41:42 2024 -0600

    another commit

commit f895c08c73a7ac258752c5489f5efc9f1f58c037
Merge: 29907a0 275cfce
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 21:30:22 2024 -0600

    Merge branch 'main' of https://gitlab.com/Hersoncpp/cs373-group-17

commit 29907a056614681831be86fc889fbe2608f0722a
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 21:30:05 2024 -0600

    another commit

commit 46687a5a9afbc3e7210219ef757c9d9a0b74f669
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 21:20:37 2024 -0600

    Navbar and Home styling

commit 275cfce8224146206995b0f34e3293f1362d15a6
Merge: 15ad768 6cc7b71
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 03:06:47 2024 +0000

    Merge branch cs373-group-17:main into main

commit 6cc7b713cbf1d888711d07edfe961d737846bd77
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 20:29:11 2024 -0600

    File Fix

commit bb17281d36e2972b6a36ceef56a0ddd2ff1506d5
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 19:58:54 2024 -0600

    Updated team.js

commit cd07532fe2c0973c3983ede7c09507b9c60db8a6
Author: Simon Stone <simonstone@Simons-MacBook-Pro.local>
Date:   Tue Feb 13 19:54:43 2024 -0600

    Added Simon picture

commit 15ad768c670ff8160fed621deca3cdac64efa37c
Merge: 9aa5fd4 010d126
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Wed Feb 14 01:12:19 2024 +0000

    Merge branch cs373-group-17:main into main

commit 9aa5fd44237df74a825b9c8fbf8b1baf2d472182
Merge: f517cba 1f8e707
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 19:11:43 2024 -0600

    county instance page done

commit 010d1262a00b2bf9fa39eca1611f813797f751c3
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 18:50:15 2024 -0600

    Fix Navbar and Restructure Team Images

commit b3d0ab0b8d4b3e9b399e78f0f6d710362220aadc
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 18:35:47 2024 -0600

    Restructure image calls

commit f517cba6f5feb185e4a6e79f1b1db51cebf73edb
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 18:15:37 2024 -0600

    county instance done

commit 1f8e70700bd3984359301e7ef699066e1ac130e0
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 17:40:50 2024 -0600

    Config File, close #41

commit cb011a189adc77a717c5e73095aac9eea76ea210
Merge: 17f7fdc 3542e52
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 17:15:15 2024 -0600

    Merge branch 'main' of gitlab.com:davindertoor77/cs373-group-17

commit 17f7fdca5ccbe58471fae9298bb74d9c16aff52f
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 17:14:49 2024 -0600

    Style Updates + Linking

commit 3542e52cc0e45775a2279f3fb17dacb1eed0f7c9
Author: simonstoneatx <simonstonesimon@gmail.com>
Date:   Tue Feb 13 17:09:46 2024 -0600

    Added county data

commit ff53b96d3f2d1efe2f893f42ad12e0c40e3a243a
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 16:41:03 2024 -0600

    Most of Types of Cancer + Some Restructuring

commit 4dbbc70ff237104cb7d2100cd222624fa2fdf360
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Tue Feb 13 12:17:42 2024 -0600

    Progress on Cancer Types

commit 4658dfc000f30dceac8ed8258c6fef3f2e2a1c1d
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Tue Feb 13 16:59:41 2024 +0000

    homepage formatting

commit f79d7385be648e03833b2fe697067a07ae62e4bc
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Tue Feb 13 16:09:42 2024 +0000

    added page titles

commit 5a850acfeb81ff5e2ab59cba43a3c6f89cacab3c
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Tue Feb 13 16:00:11 2024 +0000

    basic homepage functionality

commit 68462be4036d0831dc63b8b5c47565a2ed18e1b4
Merge: 419612d 5cb1928
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 07:35:46 2024 +0000

    Merge branch 'oncologists-instances' into 'main'
    
    Oncologists instances
    
    See merge request davindertoor77/cs373-group-17!2

commit 5cb1928c3c85ac2d68480c2c4a37ad75155e4a89
Merge: bbf16ee 022e481
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 01:34:00 2024 -0600

    Merged branch to fast forward

commit bbf16ee5969262c7110d57fafbd7a0390d3551bb
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 01:30:54 2024 -0600

    Added in 2 more dummy data instances

commit e2b82c36e47c51ef5e0f2dfd5b2bdd888d5c9250
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 01:29:14 2024 -0600

    Handles certain cases with oncologist data

commit 7985a2ab71c330b339eb15f985f99d712a55e9ae
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 01:04:01 2024 -0600

    Added pic to about page and reverted organizational changes

commit 9b2f938b0afe5fd1bac7cc0aabfbaef855fa1066
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:55:43 2024 -0600

    Finished basic oncologist page

commit 0ca1ad88731e1d7bfb29db3210ad7c989052bdf9
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:44:49 2024 -0600

    Oncologist instance page work

commit 43739ff0008ffdce2a0919be7ad4296db4dbbcf0
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:44:21 2024 -0600

    Dummy data

commit 067b714b4554e7be33e7d7714b0806cef466ee97
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:43:22 2024 -0600

    Further update to navbar and container margins

commit 5f260b1449af4046176e485c6d8155e496d7b424
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 22:47:59 2024 -0600

    Matched margins for navbar and all pages

commit 16b344793b888856f244dbd404ec63de2e459505
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 21:29:07 2024 -0600

    Reorganized about components

commit 90a5232fe724c699cb80bd7058926fd5a5c45828
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 21:24:47 2024 -0600

    Updated readme

commit 022e481c5ea4927a3581fd3533929bf31fe90caf
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 01:04:01 2024 -0600

    Added pic to about page and reverted organizational changes

commit afd0625c8bb864c7dc039ef9834b3bcf63dcd798
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:55:43 2024 -0600

    Finished basic oncologist page

commit c3b7cb41c1754724fa23a44b9e9b3b9f65552d66
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:44:49 2024 -0600

    Oncologist instance page work

commit 408cc86780402f845dccad07074e62348bfee432
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:44:21 2024 -0600

    Dummy data

commit cf31d17a97b21044634e704c5baa14769bce59b9
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Tue Feb 13 00:43:22 2024 -0600

    Further update to navbar and container margins

commit 419612d359fb1a03a1defd0832b5266f7b056f30
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 23:13:15 2024 -0600

    Merge Numan's Changes

commit d0aca24b5e7a4f75eed307a22588baea37ac3843
Merge: eed4e6b 0ac8974
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 23:06:16 2024 -0600

    Fix Merge

commit eed4e6bc2fdc0574f412f5df35891d89a190d97d
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 23:05:15 2024 -0600

    Major About update

commit 0c2f3641e73749c206ca1e620a1ebd14b0962647
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 22:47:59 2024 -0600

    Matched margins for navbar and all pages

commit 0ac897495634e1ea66f2e24be730e83d2388f7d9
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 04:13:03 2024 +0000

    Replace Herson.jpg

commit 101732d04ac5d54ead4ef57eda616cb7523eea47
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 04:12:21 2024 +0000

    Replace Herson.jpg

commit cf628e96ed880510dbf829a10baea1a3d4957a8a
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 04:11:43 2024 +0000

    Upload New File

commit 5c0bd0748d0eca93313243b593e723939e186d4e
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 04:11:30 2024 +0000

    Delete Herson.jpg

commit cdd4b079ee473b844861359c0b9a3eb1bc677d78
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:59:22 2024 +0000

    Update Team.js

commit 5de4c2941b8d82fd3f338b02fe4ba4fcff1fb904
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:58:38 2024 +0000

    Upload New File

commit e62d3450d9ec722fd188830844a014907356bd57
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:58:24 2024 +0000

    Delete Herson.jpeg

commit 5c444b6f930597c2e4a1fef666008535a3fb72e2
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:56:32 2024 +0000

    Update Team.js

commit 6443d9342bcae586faa1fc07489c087bda313ccb
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:55:11 2024 +0000

    Update Team.js

commit 1528814670140e993fa20a34ddb9b18079043cbc
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:48:51 2024 +0000

    Upload New File

commit e31b1b30c8eaf16e635cf0828a51507d4b12d93c
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:48:26 2024 +0000

    Delete self-photo.jpeg

commit b59fa08f14ccc2b4405286940ba9b71819a07140
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Tue Feb 13 03:47:57 2024 +0000

    Upload self-photo

commit aa389c2f96e0af948e518197d139727028bd4b8d
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 21:29:07 2024 -0600

    Reorganized about components

commit acb6a573774274721052bd1b564b3cabc1d2934a
Author: Donald Thai <donaldfromthailand73@gmail.com>
Date:   Mon Feb 12 21:24:47 2024 -0600

    Updated readme

commit 6d0628a6a0b0d61c1f6ce4fba8b0208933dbd571
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Tue Feb 13 01:52:04 2024 +0000

    navbar updates

commit ad7d0e71f14ba74cb6316a5e52bd38090b1806d8
Merge: a1b9e5b 6bb82b5
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 19:29:35 2024 -0600

    Merge Step

commit a1b9e5b6aefd82185e2b339ff75c06cc8e2f9d2e
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 19:28:31 2024 -0600

    Merge Step

commit 6bb82b5f1aadc3c9c277524d138513bb2b64f5e5
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Tue Feb 13 01:25:52 2024 +0000

    Added logo, headshot, and favicon

commit c12b15900cf93c5affd914c4518c1b2f876c60f8
Author: Simon Stone <simonstone@utexas.edu>
Date:   Mon Feb 12 23:32:03 2024 +0000

    Update README.md

commit 07f7b64e5177ba7960987a78f7feed817a139929
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Mon Feb 12 23:18:36 2024 +0000

    added numans information

commit d2e664e2560ed3e66a7a7f8f12cf93b4a85b75b1
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 17:02:44 2024 -0600

    Fixed README + Basic About page

commit 361c7a4df915c899a016d989802b11e8130b5582
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Mon Feb 12 21:31:35 2024 +0000

    Delete .Rhistory

commit b81b2c7a57922e241d6539d3c0b72a3f507ec90e
Merge: af23007 a8fe346
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Mon Feb 12 21:30:51 2024 +0000

    Merge branch 'main' into 'main'
    
    Adding my SID and estimation time
    
    See merge request davindertoor77/cs373-group-17!1

commit a8fe346d6eb97bb2d4726f2dbc05d73085dc3e63
Author: Hersoncpp <hwangem@connect.ust.hk>
Date:   Mon Feb 12 15:29:26 2024 -0600

    first commit

commit af23007c597a9dbb647d81a1f9c98320a05d47ab
Merge: e064ea0 4a25801
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 14:15:32 2024 -0600

    Merge branch 'main' of gitlab.com:davindertoor77/cs373-group-17

commit e064ea09d630d1fbe1f06386508a01e0d0ea60fb
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 14:15:20 2024 -0600

    Basic Page Routing/Structure

commit 4a25801b620df3eca68bf98affe6ef5ad6c65bf9
Author: Numan Osagie <nosagie@utexas.edu>
Date:   Mon Feb 12 19:37:45 2024 +0000

    added eid and time estimate

commit d9d51381177c63330e9b73e5c6a6356dfb3b17ea
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 13:00:06 2024 -0600

    Restructure MDs

commit f84e45ff8a61c4ed269099b45f6a0922b5a49a19
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 12:59:32 2024 -0600

    Restructure MDs

commit 8b1e8b8677f5fc62048338dddd6502cfd92bf460
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Mon Feb 12 12:58:21 2024 -0600

    Restructure MDs

commit 122985462c6aae940789fae08d495ede2cab110d
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Mon Feb 12 06:59:57 2024 +0000

    Update README.md

commit c337f1ace3253b1c81cd023d4e96250914e87661
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Sun Feb 11 14:30:45 2024 -0600

    Fix Build Errors

commit 0d7b33fb36a4e5f78ff8d0c6b82aef907571153a
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Sun Feb 11 14:20:24 2024 -0600

    eslint and prettier

commit 988c7852419a0ba34601f01b654dfa32c3e24212
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Sun Feb 11 14:07:33 2024 -0600

    Vulnerability Fix

commit 3c9867217dc5fd9dddc6fbabce47a1d90747ae75
Author: EmperorBob7 <40090388+EmperorBob7@users.noreply.github.com>
Date:   Sun Feb 11 02:23:44 2024 -0600

    Initialize

commit 8cf074a3a1b23f224f23921aa21fdc8aabda7a74
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Sun Feb 11 02:20:47 2024 +0000

    Update README.md

commit 7d518a3604b131726233486372dc1f179931a5ec
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Fri Feb 9 03:02:43 2024 +0000

    Update README.md

commit 7e66353259013d7a3545fbea2259c388095ae77c
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Fri Feb 9 03:00:57 2024 +0000

    Update README.md

commit 90c51338a33dc1f9fb53ac7be051c7c0d3473a18
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Fri Feb 9 02:57:17 2024 +0000

    Update README.md, Proposal

commit b10605dca7a176b40c4843f4b1ce381f9bc84706
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Mon Feb 5 19:19:44 2024 +0000

    Update README.md

commit 81a34118b8df15f79a6f6671e193d998685eadb3
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Mon Feb 5 19:19:20 2024 +0000

    Update README.md

commit 94155196200c1e8bd4d497388a1fbeef59d2abc5
Author: Davinderpal Toor <davindertoor77@gmail.com>
Date:   Mon Feb 5 19:01:01 2024 +0000

    Initial commit
